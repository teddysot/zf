/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_AMB_ZERORANGER = 836723840U;
        static const AkUniqueID PLAY_AMBIENCE = 278617630U;
        static const AkUniqueID PLAY_MUS_SOUNDTRACK = 1085672612U;
        static const AkUniqueID PLAY_MUS_STN_RACEEND = 3499519048U;
        static const AkUniqueID PLAY_MUS_STN_RACEOVER_P1 = 1256698231U;
        static const AkUniqueID PLAY_MUS_STN_RACEOVER_P4 = 1256698226U;
        static const AkUniqueID PLAY_MUS_STN_RACEOVER_P23 = 1589402031U;
        static const AkUniqueID PLAY_SFX_AMB_DRAGON = 1813387640U;
        static const AkUniqueID PLAY_SFX_AMB_GREENMONSTER = 795130680U;
        static const AkUniqueID PLAY_SFX_AMB_PARTICLEDIRECTION = 3944861538U;
        static const AkUniqueID PLAY_SFX_AMB_PUPDRONEBLADES = 2508000183U;
        static const AkUniqueID PLAY_SFX_BIKE_ENGINE = 2415867734U;
        static const AkUniqueID PLAY_SFX_BIKE_IMPACT = 2076924886U;
        static const AkUniqueID PLAY_SFX_BIKE_JUMP = 1483757620U;
        static const AkUniqueID PLAY_SFX_BIKE_LAND = 880860151U;
        static const AkUniqueID PLAY_SFX_BIKE_OFFRAMP = 2925916451U;
        static const AkUniqueID PLAY_SFX_BIKE_ONRAMP = 4090227287U;
        static const AkUniqueID PLAY_SFX_IONCANNON = 3147154451U;
        static const AkUniqueID PLAY_SFX_METALBOX_BIKE = 1976841954U;
        static const AkUniqueID PLAY_SFX_METALBOX_BOUNCE = 2844994129U;
        static const AkUniqueID PLAY_SFX_PUP_NOSBURN = 308943511U;
        static const AkUniqueID PLAY_SFX_PUP_NOSEMPTY = 2325933587U;
        static const AkUniqueID PLAY_SFX_PUP_NOSFULL = 3238464223U;
        static const AkUniqueID PLAY_SFX_PUP_NOSOFF = 824019103U;
        static const AkUniqueID PLAY_SFX_PUP_NOSOPEN = 3517645168U;
        static const AkUniqueID PLAY_SFX_PUP_SPEEDBOOST = 1181811630U;
        static const AkUniqueID PLAY_SFX_PUP_STUNMINE = 797735621U;
        static const AkUniqueID PLAY_SFX_PUP_ZEROPULSE = 1404401205U;
        static const AkUniqueID PLAY_SFX_RESPAWNBIKE = 3133641869U;
        static const AkUniqueID PLAY_SFX_SWORD_BIKE = 1900827351U;
        static const AkUniqueID PLAY_SFX_SWORD_SWING = 127354546U;
        static const AkUniqueID PLAY_SFX_SWORD_SWORD = 60538323U;
        static const AkUniqueID PLAY_SFX_TRK_LAMPPOST = 3412681994U;
        static const AkUniqueID PLAY_SFX_TRK_RING01 = 960486209U;
        static const AkUniqueID PLAY_SFX_TRK_RING02 = 960486210U;
        static const AkUniqueID PLAY_SFX_TRK_RING03 = 960486211U;
        static const AkUniqueID PLAY_SFX_TRK_SIGN_METAL = 1432962241U;
        static const AkUniqueID PLAY_SFX_TRK_SIGN_PLASTIC = 636714208U;
        static const AkUniqueID PLAY_SFX_TRK_WOOD_LRG = 163970761U;
        static const AkUniqueID PLAY_SFX_TRK_WOOD_MED = 550002974U;
        static const AkUniqueID PLAY_SFX_TRK_WOOD_SML = 4103798004U;
        static const AkUniqueID PLAY_TYP_BACKWARD = 3912634011U;
        static const AkUniqueID PLAY_TYP_FORWARD = 1238050263U;
        static const AkUniqueID PLAY_UI_CANCEL = 2244506081U;
        static const AkUniqueID PLAY_UI_CONFIRM = 3022734157U;
        static const AkUniqueID PLAY_UI_COUNTDOWN = 2883016632U;
        static const AkUniqueID PLAY_UI_FAIL = 2655186823U;
        static const AkUniqueID PLAY_UI_LAPCROSS01 = 2370906119U;
        static const AkUniqueID PLAY_UI_LAPCROSS02 = 2370906116U;
        static const AkUniqueID PLAY_UI_LAPCROSS03 = 2370906117U;
        static const AkUniqueID PLAY_UI_LAPCROSS04 = 2370906114U;
        static const AkUniqueID PLAY_UI_PUP_SHUFFLESELECT = 276629152U;
        static const AkUniqueID PLAY_UI_PUP_SHUFFLESINGLE = 3195099884U;
        static const AkUniqueID PLAY_UI_SCORE = 1297778847U;
        static const AkUniqueID PLAY_UI_SELECT = 3308548503U;
        static const AkUniqueID PLAY_UI_SLIDER_BACK = 2613479370U;
        static const AkUniqueID PLAY_UI_SLIDER_FORWARD = 1663455498U;
        static const AkUniqueID PLAY_VO_FINALLAP = 288626529U;
        static const AkUniqueID PLAY_VO_GO = 772510236U;
        static const AkUniqueID PLAY_VO_ONE = 2035018650U;
        static const AkUniqueID PLAY_VO_RACECOMPLETE = 877244712U;
        static const AkUniqueID PLAY_VO_RACESTART = 2805533811U;
        static const AkUniqueID PLAY_VO_THREE = 2701041826U;
        static const AkUniqueID PLAY_VO_TWO = 2153889084U;
        static const AkUniqueID PLAY_VO_ZERO = 2060408140U;
        static const AkUniqueID PLAY_WSH_DRONE = 3123349899U;
        static const AkUniqueID PLAY_WSH_LARGE = 1731003964U;
        static const AkUniqueID PLAY_WSH_MEDIUM = 2453091506U;
        static const AkUniqueID PLAY_WSH_OPBIKEPASSBY = 1038736001U;
        static const AkUniqueID PLAY_WSH_SMALL = 4082655748U;
    } // namespace EVENTS

    namespace DIALOGUE_EVENTS
    {
        static const AkUniqueID VO_WIPEOUT = 3150318586U;
    } // namespace DIALOGUE_EVENTS

    namespace STATES
    {
        namespace DAMAGE
        {
            static const AkUniqueID GROUP = 1786804762U;

            namespace STATE
            {
                static const AkUniqueID WIPEOUT = 1704310328U;
            } // namespace STATE
        } // namespace DAMAGE

        namespace GAMESTATE
        {
            static const AkUniqueID GROUP = 4091656514U;

            namespace STATE
            {
                static const AkUniqueID CREDITS = 2201105581U;
                static const AkUniqueID FRONTEND = 4212191649U;
                static const AkUniqueID LOBBY = 290285391U;
                static const AkUniqueID RACE = 1875627670U;
            } // namespace STATE
        } // namespace GAMESTATE

        namespace LAP
        {
            static const AkUniqueID GROUP = 578926536U;

            namespace STATE
            {
                static const AkUniqueID L1 = 1702304824U;
                static const AkUniqueID L2 = 1702304827U;
                static const AkUniqueID L3 = 1702304826U;
                static const AkUniqueID L4 = 1702304829U;
            } // namespace STATE
        } // namespace LAP

        namespace POSITION
        {
            static const AkUniqueID GROUP = 1578750580U;

            namespace STATE
            {
                static const AkUniqueID FOUR = 2863728729U;
                static const AkUniqueID ONE = 1064933119U;
                static const AkUniqueID THREE = 912956111U;
                static const AkUniqueID TWO = 678209053U;
            } // namespace STATE
        } // namespace POSITION

    } // namespace STATES

    namespace SWITCHES
    {
        namespace BIKE_LAND
        {
            static const AkUniqueID GROUP = 347459838U;

            namespace SWITCH
            {
                static const AkUniqueID JUMPLAND = 1548130050U;
                static const AkUniqueID NONLAND = 507680005U;
            } // namespace SWITCH
        } // namespace BIKE_LAND

        namespace LAP
        {
            static const AkUniqueID GROUP = 578926536U;

            namespace SWITCH
            {
                static const AkUniqueID L1 = 1702304824U;
                static const AkUniqueID L2 = 1702304827U;
                static const AkUniqueID L3 = 1702304826U;
                static const AkUniqueID L4 = 1702304829U;
            } // namespace SWITCH
        } // namespace LAP

        namespace PASSBY_SPEED
        {
            static const AkUniqueID GROUP = 3887755389U;

            namespace SWITCH
            {
                static const AkUniqueID FAST = 2965380179U;
                static const AkUniqueID MEDIUM = 2849147824U;
                static const AkUniqueID SLOW = 787604482U;
            } // namespace SWITCH
        } // namespace PASSBY_SPEED

        namespace PLAYER
        {
            static const AkUniqueID GROUP = 1069431850U;

            namespace SWITCH
            {
                static const AkUniqueID FOUR = 2863728729U;
                static const AkUniqueID ONE = 1064933119U;
                static const AkUniqueID THREE = 912956111U;
                static const AkUniqueID TWO = 678209053U;
            } // namespace SWITCH
        } // namespace PLAYER

        namespace POSITION
        {
            static const AkUniqueID GROUP = 1578750580U;

            namespace SWITCH
            {
                static const AkUniqueID P1 = 1635194252U;
                static const AkUniqueID P2 = 1635194255U;
                static const AkUniqueID P3 = 1635194254U;
                static const AkUniqueID P4 = 1635194249U;
            } // namespace SWITCH
        } // namespace POSITION

        namespace SPEED
        {
            static const AkUniqueID GROUP = 640949982U;

            namespace SWITCH
            {
                static const AkUniqueID AVERAGE = 4112839590U;
                static const AkUniqueID FAST = 2965380179U;
                static const AkUniqueID SLOW = 787604482U;
            } // namespace SWITCH
        } // namespace SPEED

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BIKE_LAND = 347459838U;
        static const AkUniqueID DISTANCE = 1240670792U;
        static const AkUniqueID LOCATION = 1176052424U;
        static const AkUniqueID ROLL = 2026920480U;
        static const AkUniqueID SPEED = 640949982U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID VOL_AMB = 2582518377U;
        static const AkUniqueID VOL_MASTER = 3391499625U;
        static const AkUniqueID VOL_MUSIC = 1004648580U;
        static const AkUniqueID VOL_SFX = 3273357900U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID ZEROFRICTION = 1790510429U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID BIKE = 1158492534U;
        static const AkUniqueID COMBAT = 2764240573U;
        static const AkUniqueID ENGINE = 268529915U;
        static const AkUniqueID GAK = 579662016U;
        static const AkUniqueID IMPACT = 3257506471U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID NONWORLD = 841296678U;
        static const AkUniqueID PASSBY = 2647161913U;
        static const AkUniqueID PUP = 913787304U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID STINGER = 78360149U;
        static const AkUniqueID TRACK = 3579833626U;
        static const AkUniqueID UI_FEEDBACK = 1260866119U;
        static const AkUniqueID VO = 1534528548U;
        static const AkUniqueID WORLD = 2609808943U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID VERB = 3657263530U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
