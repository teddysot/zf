﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var player = other.gameObject.GetComponent<PlayerManager>();

            if (player.CheckPoint < 23)
            {
                return;
            }
            else
            {
                player.SetCheckpoint(this.transform.position, this.transform.rotation, 0);
                player.AddLap(1);
            }
        }
    }
}
