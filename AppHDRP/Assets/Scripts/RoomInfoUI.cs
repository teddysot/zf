﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RoomInfoUI : MonoBehaviour
{
    [Header("GameObject References")]
    [SerializeField] private GameObject _Status;
    [SerializeField] private GameObject _Players;
    [SerializeField] private GameObject _Room;

    [Header("Info References")]
    [SerializeField] private Image _MapImage;
    [SerializeField] private TextMeshProUGUI _RoomNameText;
    [SerializeField] private TextMeshProUGUI _StatusText;
    [SerializeField] private TextMeshProUGUI _PlayersText;
    [SerializeField] private TextMeshProUGUI _MapNameText;
    [SerializeField] private GameObject _JoinButton;

    public string RoomName { get; private set; } = "";
    // Start is called before the first frame update
    void Start()
    {
        DeactivateInfo();
    }

    public void UpdateInfo(string roomName, int map, bool status, int player, int maxPlayer)
    {
        RoomName = roomName;

        _RoomNameText.text = roomName;

        if (map == 0)
        {
            _MapNameText.text = "Midnight City";
        }

        if (status)
        {
            _StatusText.text = "WAITING";
        }
        else
        {
            _StatusText.text = "PLAYING";
        }

        _PlayersText.text = $"{player}/{maxPlayer}";

        ActivateInfo();
    }

    private void ActivateInfo()
    {
        _Status.SetActive(true);
        _Players.SetActive(true);
        _Room.SetActive(true);

        _MapImage.gameObject.SetActive(true);
        _RoomNameText.gameObject.SetActive(true);
        _StatusText.gameObject.SetActive(true);
        _PlayersText.gameObject.SetActive(true);
        _MapNameText.gameObject.SetActive(true);
        _JoinButton.SetActive(true);
    }

    private void DeactivateInfo()
    {
        _Status.SetActive(false);
        _Players.SetActive(false);
        _Room.SetActive(false);

        _MapImage.gameObject.SetActive(false);
        _RoomNameText.gameObject.SetActive(false);
        _StatusText.gameObject.SetActive(false);
        _PlayersText.gameObject.SetActive(false);
        _MapNameText.gameObject.SetActive(false);
        _JoinButton.SetActive(false);
    }
}
