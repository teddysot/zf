﻿using System;
using UnityEngine;
using TMPro;
using Photon.Pun;
using UnityEngine.UI;

public class RaceTimerUI : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI _TimerText;
    [SerializeField] private Image _Image;

    [SerializeField] private Sprite[] _Sprites = new Sprite[4];

    public void UpdateWarmUpTime(int countdown)
    {
        _TimerText.gameObject.SetActive(true);
        _TimerText.text = $"Game starts in {countdown} seconds";
    }

    public void UpdatePrepareTime(int countdown)
    {
        _TimerText.gameObject.SetActive(true);
        // _TimerText.text = $"{countdown}";

        switch (countdown)
        {
            case 0:
                _Image.sprite = _Sprites[0];
                break;
            case 1:
                _Image.sprite = _Sprites[1];
                break;
            case 2:
                _Image.sprite = _Sprites[2];
                break;
            case 3:
                _Image.sprite = _Sprites[3];
                _Image.gameObject.SetActive(true);
                break;
        }
    }

    public void UpdateGameTime(int startTime)
    {
        _TimerText.gameObject.SetActive(true);
        var currentTimespan = PhotonNetwork.ServerTimestamp - startTime;
        var timespan = TimeSpan.FromMilliseconds(currentTimespan);

        if (timespan.Seconds == 1)
        {
            ClearImage();
        }

        _TimerText.text = string.Format("{0:D2}:{1:D2}:{2:D3}", timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
    }

    public void UpdateEndTime(int endTime)
    {
        _TimerText.gameObject.SetActive(true);
        _TimerText.text = $"{endTime}";
    }

    public void ClearTime()
    {
        _TimerText.text = "";
        _TimerText.gameObject.SetActive(false);
    }

    public void ClearImage()
    {
        _Image.gameObject.SetActive(false);
    }
}
