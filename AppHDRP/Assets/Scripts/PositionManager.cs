﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class PositionManager : MonoBehaviourPunCallbacks
{
    public static PositionManager Instance { get; private set; }

    [SerializeField] private GameObject[] _PlayerList = new GameObject[4];
    [SerializeField] private Transform[] _Checkpoints = new Transform[26];
    [SerializeField] private int[] _PlayerPositions = new int[4];

    [SerializeField] private float _TotalDistance = 0.0f;

    [SerializeField] private int[] _PlayerCheckpoints = new int[4];
    [SerializeField] private int[] _PlayerLaps = new int[4];
    [SerializeField] private bool[] _RaceFinish = new bool[4];

    private bool _IsInitialize = false;
    private bool _IsRaceFinish = false;
    private int _FinishCount = 0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        CalculateTotalDistance();
    }

    public void InitilizePlayerList()
    {
        foreach (var player in PhotonNetwork.PlayerList)
        {
            _PlayerList[player.ActorNumber - 1] = player.TagObject as GameObject;
            _PlayerCheckpoints[player.ActorNumber - 1] = 0;
            _PlayerLaps[player.ActorNumber - 1] = 1;
        }

        _IsInitialize = true;
    }

    private void Update()
    {
        if (_IsRaceFinish)
        {
            return;
        }

        _FinishCount = 0;
        foreach (var finish in _RaceFinish)
        {
            if (finish)
            {
                _FinishCount++;
            }
        }
    }

    private void FixedUpdate()
    {
        if (_IsInitialize == false)
        {
            return;
        }

        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            for (int j = 0; j < PhotonNetwork.PlayerList.Length; j++)
            {
                if (i == j)
                {
                    continue;
                }

                if (_PlayerCheckpoints[i] > _PlayerCheckpoints[j]
                && _PlayerPositions[i] > _PlayerPositions[j]
                && _PlayerLaps[i] == _PlayerLaps[j])
                {
                    Debug.Log($"[PositionManager] {_PlayerList[i].GetComponent<PhotonView>().Owner.NickName} just passed {_PlayerList[j].GetComponent<PhotonView>().Owner.NickName}");
                    var tempPos = _PlayerPositions[j];
                    _PlayerPositions[j] = _PlayerPositions[i];
                    _PlayerPositions[i] = tempPos;
                }

                if (_PlayerCheckpoints[i] == _PlayerCheckpoints[j]
                && _PlayerLaps[i] == _PlayerLaps[j])
                {
                    Debug.Log($"[PositionManager] {_PlayerList[i].GetComponent<PhotonView>().Owner.NickName} and {_PlayerList[j].GetComponent<PhotonView>().Owner.NickName} are on the same checkpoint!");

                    var player1 = GetNextCheckpointDistance(i);
                    var player2 = GetNextCheckpointDistance(j);

                    if (player1 < player2
                    && _PlayerPositions[i] > _PlayerPositions[j])
                    {
                        var tempPos = _PlayerPositions[j];
                        _PlayerPositions[j] = _PlayerPositions[i];
                        _PlayerPositions[i] = tempPos;
                    }
                }

                if (_PlayerLaps[i] > _PlayerLaps[j]
                && _PlayerPositions[i] > _PlayerPositions[j])
                {
                    var tempPos = _PlayerPositions[j];
                    _PlayerPositions[j] = _PlayerPositions[i];
                    _PlayerPositions[i] = tempPos;
                }
            }
        }
    }

    public void FinishRace(int playerId)
    {
        _RaceFinish[playerId - 1] = true;
    }

    public void UpdateLap(int lap, int playerId)
    {
        _PlayerLaps[playerId - 1] = lap;
    }

    public void UpdateCheckpoint(int num, int playerId)
    {
        _PlayerCheckpoints[playerId - 1] = num;
    }

    public int GetCurrentPosition(int playerId)
    {
        return _PlayerPositions[playerId - 1];
    }

    public GameObject GetFirstPositionPlayer()
    {
        for (int element = 0; element < _PlayerPositions.Length; element++)
        {
            if (_PlayerPositions[element] == 1)
            {
                return _PlayerList[element];
            }
        }

        return null;
    }

    public int GetFirstPositionLap()
    {
        int highestValue = 0;

        for (int element = 0; element < _PlayerLaps.Length; element++)
        {
            if (_PlayerLaps[element] > highestValue)
            {
                highestValue = _PlayerLaps[element];
            }
        }

        return highestValue;
    }

    private float GetNextCheckpointDistance(int playerId)
    {
        var nextCheckpoint = _PlayerCheckpoints[playerId] + 1;

        return Vector3.Distance(_PlayerList[playerId].transform.position, _Checkpoints[nextCheckpoint].position);
    }

    private void CalculateTotalDistance()
    {
        for (int i = 0; i < _Checkpoints.Length; i++)
        {
            if (i == _Checkpoints.Length - 1)
            {
                break;
            }

            _TotalDistance += Vector3.Distance(_Checkpoints[i].position, _Checkpoints[i + 1].position);
        }
    }

    public GameObject[] GetPlayers()
    {
        return _PlayerList;
    }
}
