﻿using UnityEngine;

public static class PlayerInfo
{
    public static int Bike = 0;
    public static Vector4 GammaSetting = new Vector4(1.00f, 1.00f, 1.00f, 0.00f);
    public static float MasterVolume = 100.0f;
    public static float MusicVolume = 100.0f;
    public static float SFXVolume = 100.0f;
    public static float AmbienceVolume = 100.0f;
}
