﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [Header("Combat Score Settings")]
    [SerializeField] private int _RegularAttackPoint = 100;
    [SerializeField] private int _ChargedAttackPoint = 200;
    [SerializeField] private int _KnockoffPoint = 300;
    [SerializeField] private int _BlockPoint = 250;

    [Header("PowerUp Score Settings")]
    [SerializeField] private int _StunMinePoint = 1000;
    [SerializeField] private int _IONCannonPoint = 2000;
    [SerializeField] private int _ZeroPulsePoint = 3000;

    [Header("Racing Score Settings")]
    [SerializeField] private int _DriveThroughRing = 25;
    [SerializeField] private int _MostRingsPerRace = 10000;
    [SerializeField] private int _FinishLapFirst = 2500;

    [Header("Combat Achievements Per Lap Score Settings")]
    [SerializeField] private int _MostRegularHitsLap = 2500;
    [SerializeField] private int _MostChargedHitsLap = 5000;
    [SerializeField] private int _MostBlocksLap = 1000;
    [SerializeField] private int _MostKnockoffsLap = 1250;

    [Header("Combat Achievements Per Race Score Settings")]
    [SerializeField] private int _MostRegularHitsRace = 5000;
    [SerializeField] private int _MostChargedHitsRace = 10000;
    [SerializeField] private int _MostBlocksRace = 12500;
    [SerializeField] private int _MostKnockoffsRace = 15000;

    [Header("Race Outcomes Score Settings")]
    [SerializeField] private int _FinishFirst = 50000;
    [SerializeField] private int _FinishSecond = 35000;
    [SerializeField] private int _FinishThird = 20000;
    [SerializeField] private int _FinishFourth = 10000;

    public static ScoreManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void AddScore(ref PlayerManager player, CombatType type)
    {
        switch (type)
        {
            case CombatType.REGULAR:
                player.AddScore(_RegularAttackPoint);
                break;
            case CombatType.CHARGED:
                player.AddScore(_ChargedAttackPoint);
                break;
            case CombatType.KNOCKOFF:
                player.AddScore(_KnockoffPoint);
                break;
            case CombatType.BLOCK:
                player.AddScore(_BlockPoint);
                break;
        }
    }

    public void AddScore(ref PlayerManager player, RacingType type)
    {
        switch (type)
        {
            case RacingType.RING:
                player.AddScore(_DriveThroughRing);
                break;
            case RacingType.LAPFIRST:
                player.AddScore(_FinishLapFirst);
                break;
            case RacingType.MOSTRINGS:
                player.AddScore(_MostRingsPerRace);
                break;
        }
    }

    public void AddScore(ref PlayerManager player, PowerUpType type)
    {
        switch (type)
        {
            case PowerUpType.STUNMINE:
                player.AddScore(_StunMinePoint);
                break;
            case PowerUpType.IONCANNON:
                player.AddScore(_IONCannonPoint);
                break;
            case PowerUpType.ZEROPULSE:
                player.AddScore(_ZeroPulsePoint);
                break;
        }
    }
}
