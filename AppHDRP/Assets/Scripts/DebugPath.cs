﻿using System.Collections.Generic;
using UnityEngine;

public class DebugPath : MonoBehaviour
{
    [SerializeField] private bool _RecordPath = false;
    [SerializeField] private List<Vector3> _Positions;

    [SerializeField] private float _Cooldown = 1.0f;

    private Rigidbody _BikeRB;
    private float _Timer;
    // Start is called before the first frame update
    void Start()
    {
        _Timer = _Cooldown;
        _BikeRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_RecordPath)
        {
            if (_Timer > 0.0f)
            {
                if (_BikeRB.velocity.magnitude > 1.0f)
                {
                    _Timer -= Time.deltaTime;
                }
                else
                {
                    _Timer = _Cooldown;
                }
            }
            else
            {
                _Positions.Add(gameObject.transform.position);
                _Timer = _Cooldown;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (_RecordPath)
        {
            if (_Positions.Count > 0 && _Positions != null)
            {
                for (int i = 0; i < _Positions.Count; i++)
                {
                    if (i + 1 < _Positions.Count)
                    {
                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(_Positions[i], _Positions[i + 1]);
                    }

                    Gizmos.color = Color.green;
                    Gizmos.DrawWireSphere(_Positions[i], 3.0f);
                }
            }
        }
    }

    public void SetEnable(bool value)
    {
        _RecordPath = value;
    }
}
