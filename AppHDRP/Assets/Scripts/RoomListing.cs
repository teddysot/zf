﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using Photon.Pun;

public class RoomListing : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _RoomName;
    [SerializeField] private TextMeshProUGUI _RoomPlayer;
    [SerializeField] private TextMeshProUGUI _RoomStatus;

    public RoomInfo RoomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roomInfo)
    {
        RoomInfo = roomInfo;
        _RoomName.text = $"{roomInfo.Name}";
        _RoomPlayer.text = $"{roomInfo.PlayerCount}/{roomInfo.MaxPlayers}";

        if (roomInfo.IsOpen)
        {
            _RoomStatus.text = "WAITING";
        }
        else
        {
            _RoomStatus.text = "PLAYING";
        }
    }

    public void OnSelectRoom()
    {
        var roomInfo = GameObject.FindGameObjectWithTag("RoomInfo").GetComponent<RoomInfoUI>();
        
        roomInfo.UpdateInfo(_RoomName.text ,0, RoomInfo.IsOpen, RoomInfo.PlayerCount, RoomInfo.MaxPlayers);

    }
}
