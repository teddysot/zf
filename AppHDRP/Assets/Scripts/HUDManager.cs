﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class HUDManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private PlayerNameUI _PlayerNameUI;
    [SerializeField] private PlayerOptionsUI _PlayerOptionsUI;
    [SerializeField] private PlayerPowerUpUI _PlayerPowerUpUI;
    [SerializeField] private PlayerNitrousBar _PlayerNitrousBar;
    [SerializeField] private RaceTimerUI _RacerTimerUI;
    [SerializeField] private PlayerPositionUI _PlayerPositionUI;
    [SerializeField] private PlayerLapUI _PlayerLapUI;
    [SerializeField] private PlayerSpeedMeterUI _PlayerSpeedMeterUI;

    public PlayerNameUI PlayerNameUI { get { return _PlayerNameUI; } private set { _PlayerNameUI = value; } }
    public PlayerOptionsUI PlayerOptionsUI { get { return _PlayerOptionsUI; } private set { _PlayerOptionsUI = value; } }
    public PlayerPowerUpUI PlayerPowerUpUI { get { return _PlayerPowerUpUI; } private set { _PlayerPowerUpUI = value; } }
    public PlayerNitrousBar PlayerNitrousBar { get { return _PlayerNitrousBar; } private set { _PlayerNitrousBar = value; } }
    public PlayerPositionUI PlayerPositionUI { get { return _PlayerPositionUI; } private set { _PlayerPositionUI = value; } }
    public PlayerLapUI PlayerLapUI { get { return _PlayerLapUI; } private set { _PlayerLapUI = value; } }
    public PlayerSpeedMeterUI PlayerSpeedMeterUI { get { return _PlayerSpeedMeterUI; } private set { _PlayerSpeedMeterUI = value; } }
    public RaceTimerUI RaceTimerUI { get { return _RacerTimerUI; } private set { _RacerTimerUI = value; } }

    private GameObject _HUD;

    private void Awake()
    {
        _HUD = GameObject.FindGameObjectWithTag("HUD");
        gameObject.transform.SetParent(_HUD.transform);
        var rectTransform = gameObject.GetComponent<RectTransform>();

        // Reset position of the UI
        rectTransform.anchoredPosition = Vector2.zero;
        rectTransform.sizeDelta = Vector2.zero;
        rectTransform.localPosition = Vector3.zero;
        rectTransform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
        rectTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

        _RacerTimerUI = _HUD.GetComponentInChildren<RaceTimerUI>();
        _RacerTimerUI.ClearTime();

        _PlayerOptionsUI.gameObject.SetActive(false);
        _PlayerPowerUpUI.gameObject.SetActive(false);
        _PlayerNitrousBar.gameObject.SetActive(false);
        _PlayerPositionUI.gameObject.SetActive(false);
        _PlayerLapUI.gameObject.SetActive(false);
        _PlayerSpeedMeterUI.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (LoadingScreen.Instance.IsComplete)
        {
            _PlayerNitrousBar.gameObject.SetActive(true);
            _PlayerPositionUI.gameObject.SetActive(true);
            _PlayerLapUI.gameObject.SetActive(true);
            _PlayerSpeedMeterUI.gameObject.SetActive(true);
        }
    }

    public PlayerNameUI CreateNameUI()
    {
        if (_PlayerNameUI != null)
        {
            return Instantiate(_PlayerNameUI);
        }

        return null;
    }
}
