using System;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [Header("Axis Keys Settings")]
    [SerializeField] private string _VerticalAxis = "Vertical";         // Thruster Axis
    [SerializeField] private string _HorizontalAxis = "Horizontal";     // Rudder Axis
    [SerializeField] private string _BrakeKey = "Brake";                // Brake Button
    [SerializeField] private string _AccelerateKey = "Accelerate";      // Accelerate Button
    [SerializeField] private string _ReverseKey = "Reverse";            // Reverse Button

    [Header("Input Values")]
    [SerializeField] private float _Thruster;   // Current thruster value
    [SerializeField] private float _Rudder;     // Current rudder value
    [SerializeField] private bool _IsBraking;  // Current brake value
    [SerializeField] private bool _IsJump;
    [SerializeField] private bool _IsNitrous;
    [SerializeField] private bool _IsNitrousOpen;
    [SerializeField] private bool _IsNitrousOff;
    [SerializeField] private bool _IsPowerUp;
    [SerializeField] private bool _IsFinishRace;
    [SerializeField] private bool _IsRespawn;
    [SerializeField] private bool _IsHorn;

    public float Rudder { get { return _Rudder; } private set { _Rudder = value; } }
    public float Thruster { get { return _Thruster; } private set { _Thruster = value; } }
    public bool IsBraking { get { return _IsBraking; } private set { _IsBraking = value; } }
    public bool IsJump { get { return _IsJump; } private set { _IsJump = value; } }
    public bool IsNitrous { get { return _IsNitrous; } private set { _IsNitrous = value; } }
    public bool IsNitrousOpen { get { return _IsNitrousOpen; } private set { _IsNitrousOpen = value; } }
    public bool IsNitrousOff { get { return _IsNitrousOff; } private set { _IsNitrousOff = value; } }
    public bool IsPowerUp { get { return _IsPowerUp; } private set { _IsPowerUp = value; } }
    public bool IsHorn { get { return _IsHorn; } private set { _IsHorn = value; } }
    public bool IsRespawn { get { return _IsRespawn; } private set { _IsRespawn = value; } }

    private float _Accelerate;
    private float _Reverse;
    [SerializeField] private float _VerticalInput;

    private bool _AttackRight = false;
    private bool _AttackLeft = false;
    private bool _AttackBoth = false;

    private bool _OptionsEnable = false;

    private bool _IsZeroPulse = false;

    private float _ZeroPulseDuration;
    private float _InvertDuration = 3.0f;


    private AnimationManager _AnimationManager;
    private PlayerManager _PlayerManager;
    private GameplaySFX _GameplaySFX;

    private void Start()
    {
        _AnimationManager = GetComponent<AnimationManager>();
        _PlayerManager = GetComponent<PlayerManager>();
        _GameplaySFX = GetComponent<GameplaySFX>();
    }

    private void Update()
    {
        if (LoadingScreen.Instance.IsComplete == false)
        {
            return;
        }

        if (Input.GetButtonDown("Cancel"))
        {
            OpenOptionsScreen();
        }

        if ((GameManager.Instance.PrepareStartTime != 0 && GameManager.Instance.RaceStartTime == 0) || _IsFinishRace)
        {
            ResetInput();
            return;
        }

        var optionUI = _PlayerManager.PlayerOptionsUI;

        if (optionUI != null)
        {
            _OptionsEnable = optionUI.gameObject.activeSelf;
        }

        if (_OptionsEnable == false)
        {
            CoreLogics();
        }
    }

    private void CoreLogics()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        Attacks();

        PowerUp();

        Movement();

        CheckZeroPulse();

        Horn();

        Respawn();
    }

    private void Horn()
    {
        if (Input.GetKeyDown(KeyCode.JoystickButton9) || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.RightShift))
        {
            _GameplaySFX.PlayHorn();
        }
    }

    private void Respawn()
    {
        _IsRespawn = Input.GetKey(KeyCode.R) || Input.GetKey(KeyCode.JoystickButton4);
    }

    private void PowerUp()
    {
        _IsPowerUp = Input.GetKeyDown(KeyCode.JoystickButton2) || Input.GetKeyDown(KeyCode.LeftAlt);
    }

    private void Attacks()
    {
        _AttackLeft = Input.GetMouseButton(0);
        _AttackRight = Input.GetMouseButton(1);
        _AttackBoth = _AttackLeft && _AttackRight;

        _AnimationManager.UpdateAttackAnimation(_AttackRight, _AttackLeft, _AttackBoth);
    }

    private void Movement()
    {
        // Get the values of the thruster, rudder, and brake from the input class
        _Accelerate = Input.GetAxis(_AccelerateKey) / 1.112f;
        _Reverse = Input.GetAxis(_ReverseKey) / 1.112f;
        _VerticalInput = Input.GetAxis(_VerticalAxis) / 1.112f;

        if (_VerticalInput != 0.0f)
        {
            _Thruster = _VerticalInput;
        }
        else
        {
            _Thruster = _Accelerate + _Reverse;
        }

        _Rudder = Input.GetAxis(_HorizontalAxis);
        _IsBraking = Input.GetButton(_BrakeKey) || Input.GetKey(KeyCode.JoystickButton3);
        _IsJump = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0);
        _IsNitrousOpen = Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.Joystick1Button1);
        _IsNitrousOff = Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.Joystick1Button1);

        _IsNitrous = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.JoystickButton1);

    }

    private void CheckZeroPulse()
    {
        if (_IsZeroPulse == false)
        {
            return;
        }

        _Rudder *= -1.0f;
        _ZeroPulseDuration -= Time.deltaTime;

        if (_ZeroPulseDuration <= 0.0f)
        {
            _IsZeroPulse = false;
        }
    }

    public void ActivateZeroPulse()
    {
        _IsZeroPulse = true;
        _ZeroPulseDuration = _InvertDuration;
    }

    private void OpenOptionsScreen()
    {
        if (_PlayerManager.PlayerOptionsUI.gameObject.activeSelf == false)
        {
            _PlayerManager.PlayerOptionsUI.gameObject.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            _PlayerManager.PlayerOptionsUI.gameObject.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void ResetInput()
    {
        _Thruster = 0.0f;
        _Rudder = 0.0f;
        _IsBraking = false;
        _IsJump = false;
        _IsNitrous = false;
        _IsPowerUp = false;

        _Accelerate = 0.0f;
        _Reverse = 0.0f;
    }

    public void FinishRace()
    {
        _IsFinishRace = true;
    }
}