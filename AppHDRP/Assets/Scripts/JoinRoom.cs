﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class JoinRoom : MonoBehaviourPunCallbacks
{
    [SerializeField] private RoomInfoUI _RoomInfoUI;

    private string _GameplayScene = "SCE_Gameplay";

    public void OnJoinRoomClicked()
    {
        if (_RoomInfoUI.RoomName == "")
        {
            return;
        }

        PhotonNetwork.JoinRoom(_RoomInfoUI.RoomName);
    }
}
