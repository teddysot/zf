﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingScore : MonoBehaviour
{
    private BoxCollider _Collider;
    private PlayerManager _Player;
    private float _Cooldown = 3.0f;
    private bool _IsPassed = false;

    private void Start()
    {
        _Collider = GetComponent<BoxCollider>();
    }

    private void FixedUpdate()
    {
        if (_IsPassed == false)
        {
            return;
        }

        _Cooldown -= Time.fixedDeltaTime;
        if (_Cooldown <= 0.0f)
        {
            _Collider.enabled = true;
            _IsPassed = false;
            _Cooldown = 3.0f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _Player = other.GetComponent<PlayerManager>();
            other.GetComponent<Analytics>().RingCollect();
            //ScoreManager.Instance.AddScore(ref _Player, RacingType.RING);
            _IsPassed = true;
            _Collider.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var rb = other.GetComponent<Rigidbody>();
            var force = other.transform.forward * 20.0f;
            rb.velocity += other.transform.forward * 50.0f;
            rb.velocity += other.transform.up * 60.0f;
        }
    }
}
