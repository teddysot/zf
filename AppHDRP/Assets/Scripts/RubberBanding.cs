﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RubberBanding
{
    [SerializeField] private float _DriveForce = 40.0f;
    [SerializeField] private float _MaxSpeed = 150.0f;
    [SerializeField] private float _AngleOfRoll = 30.0f;

    public float DriveForce { get { return _DriveForce; } private set { _DriveForce = value; } }
    public float MaxSpeed { get { return _MaxSpeed; } private set { _MaxSpeed = value; } }
    public float AngleOfRoll { get { return _AngleOfRoll; } private set { _AngleOfRoll = value; } }
}
