﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordBlockLogic : MonoBehaviour
{
    [SerializeField] private SFXPlayer _SFXPlayer;

    private bool _IsBlock = false;
    public bool IsBlock => _IsBlock;

    private void OnCollisionEnter(Collision other)
    {        
        if (other.gameObject.CompareTag("Sword"))
        {
            _IsBlock = true;
            _SFXPlayer.PlaySwordSword(gameObject);
            var player = transform.root.root.GetComponent<PlayerManager>();
            //ScoreManager.Instance.AddScore(ref player, CombatType.BLOCK);
        }
    }

    public void SetBlockState(bool value)
    {
        _IsBlock = value;
    }
}
