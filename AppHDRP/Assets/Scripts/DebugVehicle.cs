﻿using UnityEngine;

public class DebugVehicle : MonoBehaviour
{
    private VehicleMovement _VehicleMovement;
    private PlayerManager _PlayerManager;
    // Start is called before the first frame update
    void Start()
    {
        _VehicleMovement = GetComponent<VehicleMovement>();
        _PlayerManager = GetComponent<PlayerManager>();
    }

    public void SetDriveForce(float value)
    {
        _VehicleMovement.SetDriveForce(value);
    }

    public void SetHoverHeight(float value)
    {
        _VehicleMovement.SetHoverHeight(value);
    }

    public void SetAngleOfRoll(float value)
    {
        _VehicleMovement.SetAngleOfRoll(value);
    }

    public void SetMaxSpeed(float value)
    {
        _VehicleMovement.SetMaxSpeed(value);
    }

    public void SetMaxHoverHeight(float value)
    {
        _VehicleMovement.SetMaxHoverHeight(value);
    }

    public float GetDriveForce()
    {
        return _VehicleMovement.GetDriveForce();
    }

    public float GetHoverHeight()
    {
        return _VehicleMovement.GetHoverHeight();
    }

    public float GetAngleOfRoll()
    {
        return _VehicleMovement.GetAngleOfRoll();
    }

    public float GetSpeed()
    {
        return _VehicleMovement.GetSpeed();
    }

    public float GetMaxSpeed()
    {
        return _VehicleMovement.GetMaxSpeed();
    }

    public float GetMaxHoverHeight()
    {
        return _VehicleMovement.GetMaxHoverHeight();
    }

    public void ResetSettings()
    {
        _VehicleMovement.ResetSettings();
    }

    public void ResetGame()
    {
        _PlayerManager.Respawn();
    }
}
