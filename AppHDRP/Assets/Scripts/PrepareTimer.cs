﻿using UnityEngine;
using Photon.Pun;
using System;

public class PrepareTimer : MonoBehaviourPunCallbacks
{
    private RaceTimerUI _RaceTimerUI;

    private RaceTimer _RaceTimer;

    public delegate void PrepareTimerHasExpired();

    public static event PrepareTimerHasExpired OnPrepareTimerHasExpired;

    private bool _IsTimerRunning;

    private int _StartTime = 0;

    private VOTimerSFX _VOTimerSFX;

    private int tempCd;

    [Header("Countdown time in seconds")]
    public int Countdown = 10;

    private void Awake()
    {
        _RaceTimerUI = GetComponent<RaceTimerUI>();
        _RaceTimer = GetComponent<RaceTimer>();
        _VOTimerSFX = GetComponent<VOTimerSFX>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_IsTimerRunning == false)
        {
            return;
        }

        if (_StartTime == 0)
        {
            return;
        }

        var timer = PhotonNetwork.ServerTimestamp - _StartTime;
        var countdown = Countdown - TimeSpan.FromMilliseconds(timer).Seconds;

        if (tempCd != countdown)
        {
            tempCd = countdown;
            _VOTimerSFX.PlayVOTimer(countdown);
            _RaceTimerUI.UpdatePrepareTime(countdown);

            if (countdown == 4)
            {
                NetworkManager.Instance.LocalPlayer.GetComponent<CinematicController>().StopRaceStart();
            }
        }

        if (countdown > 0)
        {
            return;
        }

        _IsTimerRunning = false;

        if (OnPrepareTimerHasExpired != null)
        {
            OnPrepareTimerHasExpired();
        }

        _RaceTimer.RaceStart();
        this.enabled = false;
    }

    // Start is called before the first frame update
    public void PrepareStart()
    {
        GameManager.Instance.PrepareStart();

        _StartTime = GameManager.Instance.PrepareStartTime;

        NetworkManager.Instance.Spawn();

        _IsTimerRunning = true;

        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.RemovedFromList = true;
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient == false)
        {
            object prepareTime;

            if (propertiesThatChanged.TryGetValue(CustomProperties.PrepareTime, out prepareTime))
            {
                _StartTime = (int)prepareTime;

                GameManager.Instance.PrepareStartTime = _StartTime;

                Debug.Log($"[PrepareTimer] {PhotonNetwork.LocalPlayer.NickName} PrepareTime2 : {prepareTime}");
            }
        }
    }
}
