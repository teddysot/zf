﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RoomListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private Transform _Content;
    [SerializeField] private RoomListing _RoomListing;

    [SerializeField] private List<RoomListing> _RoomListings = new List<RoomListing>();

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log($"[RoomListingMenu] {PhotonNetwork.LocalPlayer.NickName} Room List Update");

        foreach (RoomInfo roomInfo in roomList)
        {
            if (roomInfo.RemovedFromList)
            {
                int index = _RoomListings.FindIndex(x => x.RoomInfo.Name == roomInfo.Name);
                Debug.Log($"[RoomListingMenu] Remove room {index}");
                if (index != -1)
                {
                    Destroy(_RoomListings[index].gameObject);
                    _RoomListings.RemoveAt(index);
                }
            }
            else
            {
                if (roomInfo.IsOpen == false)
                {
                    foreach (var room in _RoomListings)
                    {
                        if (room.RoomInfo.Name == roomInfo.Name)
                        {
                            Destroy(room.gameObject);
                            _RoomListings.Remove(room);
                            break;
                        }
                    }
                }
                RoomListing listing = Instantiate(_RoomListing, _Content);
                if (listing != null)
                {
                    listing.SetRoomInfo(roomInfo);
                    _RoomListings.Add(listing);
                }
            }
        }
    }
}
