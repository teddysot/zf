﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhooshDetector : MonoBehaviour
{
    [SerializeField] private float _TriggerTime = 0.5f;
    [SerializeField] private float _Speed = 0.0f;

    private string _PlayerTag = "Player";
    private float _Duration = 0.0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_PlayerTag))
        {
            _Speed = other.GetComponent<Rigidbody>().velocity.magnitude;
            _Duration = 0.0f;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(_PlayerTag))
        {
            _Duration += Time.deltaTime;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_PlayerTag))
        {
            if (_Duration <= _TriggerTime)
            {
                Debug.Log("Activate FastPass SFX");
            }
        }
    }
}
