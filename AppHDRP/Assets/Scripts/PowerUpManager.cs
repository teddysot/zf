﻿using System.Collections;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    private int _ShuffleTime = 0;

    private PlayerManager _Player;
    private int _Result = 0;
    private bool _Shuffle = false;

    private float _Cooldown = 0.1f;
    private float _Duration = 0.0f;

    public void GivePowerUp(GameObject player)
    {
        ShufflePowerUp(player);
    }

    private void ShufflePowerUp(GameObject player)
    {
        _ShuffleTime = 0;
        _Result = 0;
        _Player = player.GetComponent<PlayerManager>();

        _Shuffle = true;
    }

    private void Update()
    {
        if (_Shuffle == false)
        {
            return;
        }
        else
        {
            Shuffle();
        }
    }

    private void Shuffle()
    {
        if (_Duration < _Cooldown)
        {
            _Duration += Time.deltaTime;
        }
        else
        {
            _Result = Random.Range(0, 3);
            _Duration = 0.0f;
            _ShuffleTime++;
            _Player.ShufflePowerUp(_Result);

            if (_ShuffleTime == 10)
            {
                _Player.PlayLastPowerUpSFX();
            }
            else
            {
                _Player.PlayPowerUpSFX();
            }
        }

        if (_ShuffleTime == 10)
        {
            _Shuffle = false;
            _Player.CollectPowerUp((PowerUpType)_Result + 1);
        }
    }
}
