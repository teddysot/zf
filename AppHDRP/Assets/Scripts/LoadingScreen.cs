﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen Instance;

    [SerializeField] private List<Sprite> _Sprites;
    [SerializeField] private Image _BG;
    [SerializeField] private Image _Image;
    [SerializeField] private Slider _Slider;

    [SerializeField] private float _ProgressDuration = 20.0f;
    [SerializeField] private float _ImageDuration = 1.0f;

    [SerializeField] private TextMeshProUGUI _ProgressText;

    private int _SpriteIndex = 0;
    public bool IsComplete { get; private set; }

    private Sequence _Sequence;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        IsComplete = false;

        _Sequence = DOTween.Sequence();
        if (_Sprites.Count == 0)
        {
            return;
        }

        _Image.sprite = _Sprites[_SpriteIndex];

        _Sequence.Append(_Image.DOFade(0.0f, _ImageDuration).OnComplete(() =>
        {
            SlideShow();
        }));

        _Sequence.Append(_Image.DOFade(1.0f, _ImageDuration).OnComplete(() =>
        {
            SlideShow();
        }));

        _Sequence.SetLoops(-1, LoopType.Restart);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (IsComplete)
        {
            return;
        }

        _ProgressText.text = (_Slider.value * 100).ToString("F0") + "%";

        _Slider.DOValue(1.0f, _ProgressDuration).OnComplete(() =>
        {
            _Image.gameObject.SetActive(false);
            _Slider.gameObject.SetActive(false);
            _BG.gameObject.SetActive(false);
            _ProgressText.gameObject.SetActive(false);
            IsComplete = true;
        });

        if (_Sprites.Count == 0)
        {
            return;
        }

        if (_SpriteIndex == _Sprites.Count - 1)
        {
            _SpriteIndex = 0;
        }

        _Sequence.Play();
    }

    private void SlideShow()
    {
        if (_SpriteIndex == _Sprites.Count - 1)
        {
            _SpriteIndex = 0;
        }

        _SpriteIndex++;
        _Image.sprite = _Sprites[_SpriteIndex];
    }
}
