﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon.Pun;

public class Leaderboard : MonoBehaviourPunCallbacks
{
    [SerializeField] private LeaderboardSlot[] _Slots = new LeaderboardSlot[4];
    [SerializeField] private RectTransform _Panel;
    [SerializeField] private Sprite[] _BikeImages = new Sprite[8];
    [SerializeField] private GameObject _ExitButton;
    [SerializeField] private float _WaitTime = 4.0f;

    private string _RaceEndEvent = "Play_MUS_STN_RaceEnd";

    private void Awake()
    {
        _ExitButton.SetActive(false);
        this.gameObject.SetActive(false);
    }

    public void ShowBoard()
    {
        this.gameObject.SetActive(true);

        foreach (var item in GameManager.Instance.GetFinishTimes())
        {
            Debug.Log($"Loop: {item}");
        }

        for (int i = 0; i < PhotonNetwork.CurrentRoom.PlayerCount; i++)
        {
            int time = GameManager.Instance.GetFinishTimes()[i];
            int position = 0;
            for (int j = 0; j < PhotonNetwork.CurrentRoom.PlayerCount; j++)
            {
                if (i == j)
                {
                    continue;
                }
                int time2 = GameManager.Instance.GetFinishTimes()[j];

                if (time > time2 && time2 != 0)
                {
                    position++;
                }
            }

            int bikeIndex = (int)PhotonNetwork.PlayerList[i].CustomProperties[CustomProperties.Bike];
            string playerName = (string)PhotonNetwork.PlayerList[i].NickName;
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(time);
            _Slots[position].UpdatePlayerInfo(position + 1, _BikeImages[bikeIndex], playerName, timeSpan);
        }

        DOTween.To(() => _WaitTime, x => _WaitTime = x, 0.0f, 4.0f).OnComplete(() =>
        {
            AkSoundEngine.PostEvent(_RaceEndEvent, this.gameObject);
            AkSoundEngine.SetState("GameState", "Lobby");
            AnimateBoard();
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        );
    }

    private void AnimateBoard()
    {
        _Panel.DOAnchorPos(Vector2.zero, 1.0f).OnComplete(() =>
        {
            _Slots[0].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0.0f, 128.0f), 1.0f).OnComplete(() =>
            {
                if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                {
                    return;
                }

                _Slots[1].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0.0f, 28.0f), 1.0f).OnComplete(() =>
                {
                    if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
                    {
                        return;
                    }
                    _Slots[2].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0.0f, -72.0f), 1.0f).OnComplete(() =>
                    {
                        if (PhotonNetwork.CurrentRoom.PlayerCount == 3)
                        {
                            return;
                        }
                        _Slots[3].GetComponent<RectTransform>().DOAnchorPos(new Vector2(0.0f, -172.0f), 1.0f);
                    });
                });

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

            });
            _ExitButton.SetActive(true);
        });
    }

    public void OnExitClicked()
    {
        AkSoundEngine.SetState("GameState", "FrontEnd");
        PhotonNetwork.Disconnect();
    }
}
