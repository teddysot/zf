﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarSFX : MonoBehaviour
{
    private string _PillarHitEvent = "Play_SFX_TRK_Pillar_Bike";
    private string _PillarBounceEvent = "Play_SFX_TRK_Pillar_Bounce";

    [SerializeField] private LayerMask _GroundLayer;

    [SerializeField] private float collisionEnterMinValue = 2.0f;
    [SerializeField] private float collisionEnterMaxValue = 15.0f;
    [SerializeField] private float collisionStayMinValue = 2.0f;
    [SerializeField] private float collisionStayMaxValue = 15.0f;
    [SerializeField] private bool debugLogCollisionInfo;

    private float nextCollisionTime = 0.0f;
    private float impactVelocity = 0.0f;
    private float previousImpactVelocity = 0.0f;

    private static float MIN_VELOCITY = 0.0f;
    private static float MAX_VELOCITY = 1.0f;
    private static float COLLISION_WINDOW = 0.33f;

    private void OnCollisionEnter(Collision other)
    {
        float magnitude = other.relativeVelocity.magnitude;

#if UNITY_EDITOR
        if (debugLogCollisionInfo)
            Debug.Log("OnCollisionEnter, magnitude: " + magnitude, gameObject);
#endif

        if (magnitude > collisionEnterMinValue)
            Collide(magnitude);

        if (other.gameObject.CompareTag("Player"))
        {
            AkSoundEngine.PostEvent(_PillarHitEvent, this.gameObject);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Player")) return;

        float magnitude = collision.relativeVelocity.magnitude;

#if UNITY_EDITOR
        if (debugLogCollisionInfo)
            Debug.Log("OnCollisionStay, magnitude: " + magnitude, gameObject);
#endif

        if (magnitude > collisionStayMinValue)
            Collide(magnitude);
    }

    void Collide(float magnitude)
    {
        impactVelocity = ConvertVelocityRange(magnitude);

        // Time window protects against collision spamming. Only allow through collisions 
        // with velocities significantly higher than previous while window is active.
        if ((Time.time > nextCollisionTime) || ((impactVelocity - previousImpactVelocity) > ((MAX_VELOCITY - MIN_VELOCITY) * 0.33)))
        {
            nextCollisionTime = Time.time + COLLISION_WINDOW;

            AkSoundEngine.PostEvent(_PillarBounceEvent, this.gameObject);

            previousImpactVelocity = impactVelocity;
        }
    }

    float ConvertVelocityRange(float inputValue)
    {
        if (inputValue > collisionEnterMaxValue)
            inputValue = collisionEnterMaxValue;
        if (inputValue < collisionEnterMinValue)
            inputValue = collisionEnterMinValue;

        if (collisionEnterMinValue > collisionEnterMaxValue)
            Debug.LogWarning("AudioPhysicsCollision: Object's audio collision range Min > Max!", gameObject);

        float inputRange = (collisionEnterMaxValue - collisionEnterMinValue);
        float outputRange = (MAX_VELOCITY - MIN_VELOCITY);
        float outputValue = (((inputValue - collisionEnterMinValue) * outputRange) / inputRange) + MIN_VELOCITY;

        if (outputValue > MAX_VELOCITY)
            outputValue = MAX_VELOCITY;
        if (outputValue < MIN_VELOCITY)
            outputValue = MIN_VELOCITY;

#if UNITY_EDITOR
        if (debugLogCollisionInfo)
            Debug.Log("Converted velocity: " + outputValue, gameObject);
#endif

        return outputValue;
    }
}
