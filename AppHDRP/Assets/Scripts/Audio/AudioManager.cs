﻿using UnityEngine;
using Photon.Pun;

public class AudioManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private bool _IsMainMenu = false;

    [SerializeField] private bool _IsLobbyPlay = false;
    [SerializeField] private bool _IsPreparePlay = false;
    [SerializeField] private bool _IsRacePlay = false;

    private string _PlaySoundtrackEvent = "Play_MUS_Soundtrack";
    [SerializeField] private AK.Wwise.Bank _SoundBank;

    private GameObject[] _Players = { null, null, null, null };

    private int _CurrentFirstPositionLap = 0;

    private int counter = 0;

    private void Awake()
    {
        while (counter < 1)
        {
            _SoundBank.Load();

            if (_SoundBank.Validate())
            {
                Debug.Log("Soundbank is valid");
            }
            counter++;
        }
    }

    private void Start()
    {
        PlayMenuMusic();
    }

    private void Update()
    {
        if (_IsMainMenu)
        {
            return;
        }

        PlayLobbyMusic();
        PlayRaceMusic();
        UpdateLap();
    }

    private void UpdateLap()
    {
        if (_IsRacePlay)
        {
            if (_CurrentFirstPositionLap < PositionManager.Instance.GetFirstPositionLap())
            {
                _CurrentFirstPositionLap = PositionManager.Instance.GetFirstPositionLap();

                switch (PositionManager.Instance.GetFirstPositionLap())
                {
                    case 1: AkSoundEngine.SetState("Lap", "L1"); break;
                    case 2: AkSoundEngine.SetState("Lap", "L2"); break;
                    case 3: AkSoundEngine.SetState("Lap", "L3"); break;
                    case 4: AkSoundEngine.SetState("Lap", "L4"); break;
                    default: Debug.Log($"[{this.ToString()}] Couldn't switch Laps for Music"); break;
                }
            }
        }
    }

    private void PlayMenuMusic()
    {
        if (_IsMainMenu)
        {
            AkSoundEngine.PostEvent(_PlaySoundtrackEvent, this.gameObject);
            AkSoundEngine.SetState("GameState", "FrontEnd");
        }
    }

    private void PlayLobbyMusic()
    {
        if (LoadingScreen.Instance.IsComplete && _IsLobbyPlay == false)
        {
            AkSoundEngine.SetState("GameState", "Lobby");
            _IsLobbyPlay = true;
        }
    }

    private void PlayRaceMusic()
    {
        if (GameManager.Instance.RaceStartTime != 0 && _IsRacePlay == false)
        {
            AkSoundEngine.SetState("GameState", "Race");
            Debug.Log("CHANGED TO RACE MUSIC");
            _IsRacePlay = true;
        }
    }
}
