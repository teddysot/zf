﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbiencePlayer : MonoBehaviour
{
    [SerializeField] private bool _playOnStart = true;
    [SerializeField] private string _AmbiencePlayEvent = null;

    // Mono
    void Start()
    {
        if (_playOnStart)
        {
            PlayAmbience();
        }
    }

    // Play Ambience
    public void PlayAmbience()
    {
        AkSoundEngine.PostEvent(_AmbiencePlayEvent, this.gameObject);
    }

    // Pause Ambience
    public void PauseAmbience()
    {

    }

    // Stop Ambience
    public void StopAmbience()
    {

    }
}
