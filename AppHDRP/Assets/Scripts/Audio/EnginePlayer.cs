﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnginePlayer : MonoBehaviour
{
    private string _EngineEventName = "Play_SFX_Bike_Engine";

    public void SetEngineSpeed(float speed)
    {
        var spd = speed * 100;
        AkSoundEngine.SetRTPCValue("Speed", spd , gameObject);
    }

    public void SetEngineRoll(float value)
    {
        AkSoundEngine.SetRTPCValue("Roll", Mathf.Abs(value) , this.gameObject);
    }

    public void PlayEngineSound(){
        AkSoundEngine.PostEvent(_EngineEventName, gameObject);
    }
}
