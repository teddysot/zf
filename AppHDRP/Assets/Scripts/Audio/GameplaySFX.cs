﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplaySFX : MonoBehaviour
{
    private string _LapCross1 = "Play_UI_LapCross01";
    private string _LapCross2 = "Play_UI_LapCross02";
    private string _LapCross3 = "Play_UI_LapCross03";
    private string _LapCross4 = "Play_UI_LapCross04";

    private string _BikeImpact = "Play_SFX_Bike_Impact";
    private string _IonCannon = "Play_SFX_IonCannon";

    private string _BikeJump = "Play_SFX_Bike_Jump";
    private string _BikeLand = "Play_SFX_Bike_Land";
    private string _BikeOnRamp = "Play_SFX_Bike_OnRamp";
    private string _BikeOffRamp = "Play_SFX_Bike_OffRamp";
    private string _RespawnBike = "Play_SFX_RespawnBike";
    private string _Horn = "Play_SFX_Bike_Horn";

    private string _MetalSignEvent = "Play_SFX_TRK_Sign_Metal";
    private string _PlasticSignEvent = "Play_SFX_TRK_Sign_Plastic";
    private string _ConcreteEvent = "Play_SFX_TRK_Concrete";

    private string _NOSEmpty = "Play_SFX_PUP_NOSEmpty";
    private string _NOSBurn = "Play_SFX_PUP_NOSBurn";
    private string _NOSStop = "Stop_SFX_PUP_NOSBurn";
    private string _NOSOpen = "Play_SFX_PUP_NOSOpen";
    private string _NOSFull = "Play_SFX_PUP_NOSFull";
    private string _NOSOff = "Play_SFX_PUP_NOSOff";
    private string _NOSPickup = "Play_SFX_PUP_NOSPickup";
    private string _SpeedBoost = "Play_SFX_PUP_SpeedBoost";
    private string _ZeroPulse = "Play_SFX_PUP_ZeroPulse";
    private string _PowerUpShuffleSFX = "Play_UI_PUP_ShuffleSingle";
    private string _LastPowerUpShuffleSFX = "Play_UI_PUP_ShuffleSelect";
    private string _StunMineHit = "Play_SFX_PUP_StunMineExplo";

    private string _RaceEnd = "Play_MUS_STN_RaceEnd";
    private string _RaceOverP1 = "Play_MUS_STN_RaceOver_P1";
    private string _RaceOverP23 = "Play_MUS_STN_RaceOver_P23";
    private string _RaceOverP4 = "Play_MUS_STN_RaceOver_P4";

    public void PlayHorn()
    {
        AkSoundEngine.PostEvent(_Horn, this.gameObject);
    }

    public void PlayLapSFX(int lap)
    {
        switch (lap)
        {
            case 1:
                AkSoundEngine.PostEvent(_LapCross1, this.gameObject);
                break;
            case 2:
                AkSoundEngine.PostEvent(_LapCross2, this.gameObject);
                break;
            case 3:
                AkSoundEngine.PostEvent(_LapCross4, this.gameObject);
                break;
            case 4:
                AkSoundEngine.PostEvent(_LapCross4, this.gameObject);
                break;
        }
    }

    public void PlayPUP(int value)
    {
        switch (value)
        {
            case 1:
                AkSoundEngine.PostEvent(_NOSEmpty, this.gameObject);
                break;
            case 2:
                AkSoundEngine.PostEvent(_NOSFull, this.gameObject);
                break;
            case 3:
                AkSoundEngine.PostEvent(_SpeedBoost, this.gameObject);
                break;
            case 4:
                AkSoundEngine.PostEvent(_ZeroPulse, this.gameObject);
                break;
            case 5:
                AkSoundEngine.PostEvent(_IonCannon, this.gameObject);
                break;
            case 6:
                AkSoundEngine.PostEvent(_NOSPickup, this.gameObject);
                break;
        }
    }

    public void PlayStinger(int value)
    {
        switch (value)
        {
            case 0:
                AkSoundEngine.PostEvent(_RaceEnd, this.gameObject);
                break;
            case 1:
                AkSoundEngine.PostEvent(_RaceOverP1, this.gameObject);
                break;
            case 2:
            case 3:
                AkSoundEngine.PostEvent(_RaceOverP23, this.gameObject);
                break;
            case 4:
                AkSoundEngine.PostEvent(_RaceOverP4, this.gameObject);
                break;
        }

        Debug.Log($"[GameplaySFX] Play Stinger: {value}");
    }

    public void PlayMetalSign()
    {
        AkSoundEngine.PostEvent(_MetalSignEvent, this.gameObject);
    }

    public void PlayPlasticSign()
    {
        AkSoundEngine.PostEvent(_PlasticSignEvent, this.gameObject);
    }

    public void PlayConcrete()
    {
        AkSoundEngine.PostEvent(_ConcreteEvent, this.gameObject);
    }

    public void PlayStunMineHit()
    {
        AkSoundEngine.PostEvent(_StunMineHit, this.gameObject);
    }

    public void PlayImpact()
    {
        AkSoundEngine.PostEvent(_BikeImpact, this.gameObject);
    }

    public void PlayJump()
    {
        AkSoundEngine.PostEvent(_BikeJump, this.gameObject);
    }

    public void PlayLand()
    {
        AkSoundEngine.PostEvent(_BikeLand, this.gameObject);
    }

    public void PlayOnRamp()
    {
        AkSoundEngine.PostEvent(_BikeOnRamp, this.gameObject);
    }

    public void PlayOffRamp()
    {
        AkSoundEngine.PostEvent(_BikeOffRamp, this.gameObject);
    }

    public void PlayRespawn()
    {
        AkSoundEngine.PostEvent(_RespawnBike, this.gameObject);
    }

    public void PlayPowerUpShuffle()
    {
        AkSoundEngine.PostEvent(_PowerUpShuffleSFX, this.gameObject);
    }

    public void PlayLastPowerUpShuffle()
    {
        AkSoundEngine.PostEvent(_LastPowerUpShuffleSFX, this.gameObject);
    }

    public void PlayNOSOpen()
    {
        AkSoundEngine.PostEvent(_NOSOpen, this.gameObject);
    }

    public void PlayNOSOff()
    {
        AkSoundEngine.PostEvent(_NOSOff, this.gameObject);
    }

    public void PlayNOSBurn()
    {
        AkSoundEngine.PostEvent(_NOSBurn, this.gameObject);
    }

    public void StopNOS()
    {
        AkSoundEngine.PostEvent(_NOSStop, this.gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayImpact();
        }
    }
}
