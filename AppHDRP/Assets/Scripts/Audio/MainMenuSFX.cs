﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainMenuSFX : MonoBehaviour
{
    [SerializeField] private string _CancelSFX = "Play_UI_Cancel";
    [SerializeField] private string _ConfirmSFX = "Play_UI_Confirm";
    [SerializeField] private string _FailSFX = "Play_UI_Fail";
    [SerializeField] private string _SelectSFX = "Play_UI_Select";
    [SerializeField] private string _SliderBackSFX = "Play_UI_Slider_Back";
    [SerializeField] private string _SliderForwardSFX = "Play_UI_Slider_Forward";

    [SerializeField] private TextMeshProUGUI _MasterValueText;
    [SerializeField] private TextMeshProUGUI _MusicValueText;
    [SerializeField] private TextMeshProUGUI _SFXValueText;
    [SerializeField] private TextMeshProUGUI _AmbienceValueText;

    private float _MasterValue = 100.0f;
    private float _MusicValue = 100.0f;
    private float _SFXValue = 100.0f;
    private float _AmbienceValue = 100.0f;

    private float _GammaValue = 1.0f;

    public void PlayCancelSFX()
    {
        AkSoundEngine.PostEvent(_CancelSFX, this.gameObject);
    }

    public void PlayConfirmSFX()
    {
        AkSoundEngine.PostEvent(_ConfirmSFX, this.gameObject);
    }

    public void PlayFailSFX()
    {
        AkSoundEngine.PostEvent(_FailSFX, this.gameObject);
    }

    public void PlaySelectSFX()
    {
        AkSoundEngine.PostEvent(_SelectSFX, this.gameObject);
    }

    public void PlayMasterSliderSFX(float value)
    {
        if (value < _MasterValue)
        {
            AkSoundEngine.PostEvent(_SliderBackSFX, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_SliderForwardSFX, this.gameObject);
        }
        AkSoundEngine.SetRTPCValue("VOL_Master", value, this.gameObject);
        PlayerInfo.MasterVolume = value;
        _MasterValue = value;
        _MasterValueText.text = ((int)_MasterValue).ToString();
    }

    public void PlayMusicSliderSFX(float value)
    {
        if (value < _MusicValue)
        {
            AkSoundEngine.PostEvent(_SliderBackSFX, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_SliderForwardSFX, this.gameObject);
        }
        AkSoundEngine.SetRTPCValue("VOL_Music", value, this.gameObject);
        PlayerInfo.MusicVolume = value;
        _MusicValue = value;
        _MusicValueText.text = ((int)_MusicValue).ToString();
    }

    public void PlaySFXSliderSFX(float value)
    {
        if (value < _SFXValue)
        {
            AkSoundEngine.PostEvent(_SliderBackSFX, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_SliderForwardSFX, this.gameObject);
        }
        AkSoundEngine.SetRTPCValue("VOL_SFX", value, this.gameObject);
        AkSoundEngine.SetRTPCValue("VOL_UI", value, this.gameObject);
        PlayerInfo.SFXVolume = value;
        _SFXValue = value;
        _SFXValueText.text = ((int)_SFXValue).ToString();
    }

    public void PlayAmbienceSliderSFX(float value)
    {
        if (value < _AmbienceValue)
        {
            AkSoundEngine.PostEvent(_SliderBackSFX, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_SliderForwardSFX, this.gameObject);
        }
        AkSoundEngine.SetRTPCValue("VOL_Amb", value, this.gameObject);
        PlayerInfo.AmbienceVolume = value;
        _AmbienceValue = value;
        _AmbienceValueText.text = ((int)_AmbienceValue).ToString();
    }

    public void PlayGammaSliderSFX(float value)
    {
        if (value < _GammaValue)
        {
            AkSoundEngine.PostEvent(_SliderBackSFX, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_SliderForwardSFX, this.gameObject);
        }
        _GammaValue = value;
    }
}
