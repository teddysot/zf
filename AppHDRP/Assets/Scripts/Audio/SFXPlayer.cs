﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    [Header("Environment")]
    private string _ScoreSFX = "Play_UI_Score";
    private string _Ring1SFX = "Play_SFX_TRK_Ring01";
    private string _Ring2SFX = "Play_SFX_TRK_Ring02";
    private string _Ring3SFX = "Play_SFX_TRK_Ring03";

    [Header("Sword SFXs")]
    private string _SwordBikeSFX = "Play_SFX_Sword_Bike";
    private string _SwordSwingSFX = "Play_SFX_Sword_Swing";
    private string _SwordSwordSFX = "Play_SFX_Sword_Sword";

    public void PlayScoreSFX(GameObject target)
    {
        AkSoundEngine.PostEvent(_ScoreSFX, target);
    }

    public void PlaySwordBike(GameObject target)
    {
        AkSoundEngine.PostEvent(_SwordBikeSFX, target);
    }

    public void PlaySwordSwing(GameObject target)
    {
        AkSoundEngine.PostEvent(_SwordSwingSFX, target);
    }

    public void PlaySwordSword(GameObject target)
    {
        AkSoundEngine.PostEvent(_SwordSwordSFX, target);
    }

    public void PlayRingSFX(int ring)
    {
        switch (ring)
        {
            case 1:
                AkSoundEngine.PostEvent(_Ring1SFX, this.gameObject);
                break;
            case 2:
                AkSoundEngine.PostEvent(_Ring2SFX, this.gameObject);
                break;
            case 3:
                AkSoundEngine.PostEvent(_Ring3SFX, this.gameObject);
                break;
        }
    }
}
