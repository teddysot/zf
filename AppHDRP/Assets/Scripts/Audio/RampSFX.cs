﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RampSFX : MonoBehaviour
{
    [SerializeField] private bool _OnRamp;
    [SerializeField] private bool _OffRamp;

    private string _OnRampEvent = "Play_SFX_Bike_OnRamp";
    private string _OffRampEvent = "Play_SFX_Bike_OffRamp";

    private void OnTriggerEnter(Collider other) 
    {
        if(other.CompareTag("Player"))
        {
            if(_OnRamp)
            {
                AkSoundEngine.PostEvent(_OnRampEvent, this.gameObject);
            }

            if(_OffRamp)
            {
                AkSoundEngine.PostEvent(_OffRampEvent, this.gameObject);
            }
        }
    }
}
