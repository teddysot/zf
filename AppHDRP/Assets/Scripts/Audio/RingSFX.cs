﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingSFX : MonoBehaviour
{
    [SerializeField] private int _NumRing = 1;

    private string _1stRingEvent = "Play_SFX_TRK_Ring01";
    private string _2ndRingEvent = "Play_SFX_TRK_Ring02";
    private string _3rdRingEvent = "Play_SFX_TRK_Ring03";

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            switch (_NumRing)
            {
                case 1:
                    AkSoundEngine.PostEvent(_1stRingEvent, this.gameObject);
                    break;
                case 2:
                    AkSoundEngine.PostEvent(_2ndRingEvent, this.gameObject);
                    break;
                case 3:
                    AkSoundEngine.PostEvent(_3rdRingEvent, this.gameObject);
                    break;
            }
        }
    }
}
