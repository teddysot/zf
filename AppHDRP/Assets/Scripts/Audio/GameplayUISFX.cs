﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayUISFX : MonoBehaviour
{
    private string _CancelSFX = "Play_UI_Cancel";
    private string _ConfirmSFX = "Play_UI_Confirm";
    private string _FailSFX = "Play_UI_Fail";
    private string _SelectSFX = "Play_UI_Select";
    private string _SliderBackSFX = "Play_UI_Slider_Back";
    private string _SliderForwardSFX = "Play_UI_Slider_Forward";

    private float _MasterValue = 100.0f;
    private float _MusicValue = 100.0f;
    private float _SFXValue = 100.0f;
    private float _AmbienceValue = 100.0f;

    private float _GammaValue = 1.0f;

    private void Start()
    {
        _GammaValue = PlayerInfo.GammaSetting.x;
    }

    public void PlayCancelSFX()
    {
        AkSoundEngine.PostEvent(_CancelSFX, this.gameObject);
    }

    public void PlayConfirmSFX()
    {
        AkSoundEngine.PostEvent(_ConfirmSFX, this.gameObject);
    }

    public void PlayFailSFX()
    {
        AkSoundEngine.PostEvent(_FailSFX, this.gameObject);
    }

    public void PlaySelectSFX()
    {
        AkSoundEngine.PostEvent(_SelectSFX, this.gameObject);
    }

    public void PlayGammaSliderSFX(float value)
    {
        if (value < _GammaValue)
        {
            AkSoundEngine.PostEvent(_SliderBackSFX, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_SliderForwardSFX, this.gameObject);
        }
        _GammaValue = value;
    }
}
