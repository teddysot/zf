﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceAmb : MonoBehaviour
{
    private string _AudienceAmbience = "Play_SFX_AMB_Robot";
    // Start is called before the first frame update
    void Start()
    {
        AkSoundEngine.PostEvent(_AudienceAmbience, this.gameObject);
    }
}
