﻿using UnityEngine;

public class WooshSFX : MonoBehaviour
{
    private string _WSHDrone = "Play_WSH_Drone";
    private string _WSHLarge = "Play_WSH_Large";
    private string _WSHMedium = "Play_WSH_Medium";
    private string _WSHSmall = "Play_WSH_Small";

    [SerializeField] private bool _IsDrone = false;
    [SerializeField] private bool _IsLarge = false;
    [SerializeField] private bool _IsMedium = false;
    [SerializeField] private bool _IsSmall = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // RTPC
            float speed = other.GetComponent<VehicleMovement>().GetSpeedPercentage() * 100;
            //Debug.Log($"{this.ToString()} | Speed = {speed}");
            AkSoundEngine.SetRTPCValue("Speed", speed, this.gameObject);

            // Events
            if (_IsDrone)
            {
                PlayDrone();
            }

            if (_IsLarge)
            {
                PlayWSHLarge();
            }

            if (_IsMedium)
            {
                PlayWSHMedium();
            }

            if (_IsSmall)
            {
                PlayWSHSmall();
            }
        }
    }

    public void PlayDrone()
    {
        AkSoundEngine.PostEvent(_WSHDrone, this.gameObject);
    }

    public void PlayWSHLarge()
    {
        AkSoundEngine.PostEvent(_WSHLarge, this.gameObject);
    }

    public void PlayWSHMedium()
    {
        AkSoundEngine.PostEvent(_WSHMedium, this.gameObject);
    }

    public void PlayWSHSmall()
    {
        AkSoundEngine.PostEvent(_WSHSmall, this.gameObject);
    }
}
