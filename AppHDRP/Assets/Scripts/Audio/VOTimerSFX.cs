﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VOTimerSFX : MonoBehaviour
{
    [Header("Voice Over")]
    private string _VOGo = "Play_VO_Go";
    private string _VOOne = "Play_VO_One";
    private string _VOTwo = "Play_VO_Two";
    private string _VOThree = "Play_VO_Three";
    private string _VOZero = "Play_VO_Zero";

    public void PlayVOTimer(int value)
    {
        switch (value)
        {
            case 0:
                AkSoundEngine.PostEvent(_VOGo, this.gameObject);
                break;
            case 1:
                AkSoundEngine.PostEvent(_VOOne, this.gameObject);
                break;
            case 2:
                AkSoundEngine.PostEvent(_VOTwo, this.gameObject);
                break;
            case 3:
                AkSoundEngine.PostEvent(_VOThree, this.gameObject);
                break;
        }
    }
}
