﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortcutSFX : MonoBehaviour
{
    private void Start()
    {
        AkSoundEngine.PostEvent("Play_SFX_AMB_Shortcut", this.gameObject);
    }
}
