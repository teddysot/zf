﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignSFX : MonoBehaviour
{
    [SerializeField] private bool _MetalSign = false;
    [SerializeField] private bool _PlasticSign = false;
    [SerializeField] private bool _Concrete = false;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var playerSFX = other.gameObject.GetComponent<GameplaySFX>();
            if (_MetalSign)
            {
                playerSFX.PlayMetalSign();
            }

            if (_PlasticSign)
            {
                playerSFX.PlayPlasticSign();
            }

            if (_Concrete)
            {
                playerSFX.PlayConcrete();
            }
        }
    }
}
