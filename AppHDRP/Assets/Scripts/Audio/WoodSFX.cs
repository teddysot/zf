﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodSFX : MonoBehaviour
{
    [SerializeField] private bool _Large = false;
    [SerializeField] private bool _Medium = false;
    [SerializeField] private bool _Small = false;

    private string _LargeEvent = "Play_SFX_TRK_Wood_Lrg";
    private string _MediumEvent = "Play_SFX_TRK_Wood_Med";
    private string _SmallEvent = "Play_SFX_TRK_Wood_Sml";

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (_Large)
            {
                AkSoundEngine.PostEvent(_LargeEvent, this.gameObject);
            }

            if (_Medium)
            {
                AkSoundEngine.PostEvent(_MediumEvent, this.gameObject);
            }

            if (_Small)
            {
                AkSoundEngine.PostEvent(_SmallEvent, this.gameObject);
            }
        }
    }
}
