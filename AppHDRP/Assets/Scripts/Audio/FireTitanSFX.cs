﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTitanSFX : MonoBehaviour
{
    private string _FireTitanEvent = "Play_SFX_AMB_GreenMonster";
    // Start is called before the first frame update
    void Start()
    {
        AkSoundEngine.PostEvent(_FireTitanEvent, this.gameObject);
    }
}
