﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampPostSFX : MonoBehaviour
{
    private string _LampPostDamageSFX = "Play_SFX_TRK_LampPost";
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AkSoundEngine.PostEvent(_LampPostDamageSFX, this.gameObject);
            GetComponent<Rigidbody>().freezeRotation = false;
        }
    }
}
