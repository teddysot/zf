﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSFX : MonoBehaviour
{
    private string _ParticleEvent = "Play_SFX_AMB_ParticleDirection";
    // Start is called before the first frame update
    void Start()
    {
        AkSoundEngine.PostEvent(_ParticleEvent, this.gameObject);
    }
}
