﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchingDragonAmb : MonoBehaviour
{
    private string _GlitchingDragonEvent = "Play_SFX_AMB_Dragon";
    // Start is called before the first frame update
    void Start()
    {
        AkSoundEngine.PostEvent(_GlitchingDragonEvent, this.gameObject);
    }
}
