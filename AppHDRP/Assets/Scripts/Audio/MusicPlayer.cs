﻿using UnityEngine;
using System.Collections.Generic;

public class MusicPlayer : MonoBehaviour
{
    public static MusicPlayer Instance;

    [SerializeField] private List<string> _PlayMusicEvents;
    [SerializeField] private List<string> _StopMusicEvents;

    // To be implemented. Use it with SetSwitch and/or SetState
    public void PlayMusic(int index)
    {
        AkSoundEngine.PostEvent(_PlayMusicEvents[index], this.gameObject);
    }

    public void StopMusic(int index)
    {
        AkSoundEngine.PostEvent(_StopMusicEvents[index], this.gameObject);
    }
}
