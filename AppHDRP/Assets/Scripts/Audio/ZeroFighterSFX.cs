﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroFighterSFX : MonoBehaviour
{
    private string _AmbZeroRanger = "Play_SFX_AMB_ZeroFighter";
    // Start is called before the first frame update
    void Start()
    {
        AkSoundEngine.PostEvent(_AmbZeroRanger, this.gameObject);
    }
}
