﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    [Header("Spawner Type")]
    [SerializeField] private bool _NitrousPickup = false;
    [SerializeField] private bool _PowerUpPickup = false;
    [SerializeField] private float _Cooldown = 5.0f;

    [Header("Prefab Reference")]
    [SerializeField] private GameObject _PowerUpPrefab;
    [SerializeField] private GameObject _NitrousPrefab;

    private GameObject _GORef;
    private float _Duration;
    // Start is called before the first frame update
    void Start()
    {
        InstantiatePickup();
        _Duration = _Cooldown;
    }

    // Update is called once per frame
    void Update()
    {
        if (_GORef != null)
        {
            return;
        }

        _Duration -= Time.deltaTime;

        if (_Duration <= 0.0f)
        {
            InstantiatePickup();
            _Duration = _Cooldown;
        }
    }

    private void InstantiatePickup()
    {
        if (_NitrousPickup)
        {
            _GORef = Instantiate(_NitrousPrefab, transform.position, transform.rotation);
        }

        if (_PowerUpPickup)
        {
            _GORef = Instantiate(_PowerUpPrefab, transform.position, transform.rotation);
        }

        if (_GORef != null)
        {
            _GORef.transform.SetParent(this.gameObject.transform);
            _GORef.transform.localPosition = Vector3.zero;
        }
    }
}
