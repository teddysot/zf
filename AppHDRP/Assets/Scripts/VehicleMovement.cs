using Photon.Pun;
using UnityEngine;

public class VehicleMovement : MonoBehaviourPun
{
    [SerializeField] private float _Speed;                          // Current Speed of the vehicle
    public float Speed { get { return _Speed; } private set { _Speed = value; } }

    [Header("Rubber Banding")]
    [SerializeField] private RubberBanding _FirstRB;
    [SerializeField] private RubberBanding _SecondRB;
    [SerializeField] private RubberBanding _ThirdRB;
    [SerializeField] private RubberBanding _FourthRB;

    [Header("Drive Settings")]
    [SerializeField] private float _DriveForce = 17.0f;             // Force that the engine generates
    [SerializeField] private float _SlowingVelFactor = 0.999f;      // Decelerate Amount
    [SerializeField] private float _BrakingVelFactor = 0.99f;       // Brake Decelerate Amount
    [SerializeField] private float _AngleOfRoll = 30.0f;            // Angle of the ship banks into a turn
    [SerializeField] private float _Angle = 0.0f;

    [Header("Hover Settings")]
    [SerializeField] private float _HoverHeight = 1.5f;             // The height of the ship maintains while hovering
    [SerializeField] private float _MaxGroundDist = 5.0f;           // The distance the ship can be above ground before it is falling
    [SerializeField] private float _HoverForce = 300.0f;            // The force of the ship's hovering
    [SerializeField] private LayerMask _GroundLayer;                // Determine ground layer
    [SerializeField] private LayerMask _WallLayer;                  // Determin Wall layer
    [SerializeField] private PIDController _HoverPID;               // PID Controller

    [Header("Physics Settings")]
    [SerializeField] private Transform _VehicleBody;                // References to the vehicle's body
    [SerializeField] private float _TerminalVelocity = 100.0f;      // Max speed
    [SerializeField] private float _HoverGravity = 20.0f;           // Gravity applied to the ship while it is on the ground
    [SerializeField] private float _FallGravity = 80.0f;            // Gravity applied to the ship while it is falling

    [SerializeField] private float _JumpForce = 100.0f;
    [SerializeField] private float _JetPackForce = 50.0f;

    private Rigidbody _Rigidbody;                                   // Reference to the vehicle's rigidbody
    private PlayerInput _PlayerInput;                               // Reference to the player's input
    private NitrousSystem _NitrousSystem;
    private AnimationManager _AnimationManager;
    private GameplaySFX _GameplaySFX;
    private PostProcessingController _PPController;
    private float drag;                                             // Air resistance the ship receives in the forward direction

    [SerializeField] private bool _IsOnGround = false;              // A flag determining if the ship is currently on the ground

    private float _prevDF;
    private float _prevAOR;
    private float _prevHH;
    private float _prevTV;
    private float _prevMHH;

    private bool _IsZeroBolt = false;
    private float _ZeroBoltDuration;
    private float _BoltDuration = 3.0f;

    private float _InAirDuration = 0.0f;

    private float _BoostEffectCooldown = 0.01f;
    private bool _IsBoost = false;

    private void Start()
    {
        // Get references to the Rigidbody and PlayerInput Component
        _Rigidbody = GetComponent<Rigidbody>();
        _PlayerInput = GetComponent<PlayerInput>();
        _AnimationManager = GetComponent<AnimationManager>();
        _NitrousSystem = GetComponent<NitrousSystem>();
        _GameplaySFX = GetComponent<GameplaySFX>();
        _PPController = GetComponent<PostProcessingController>();

        // Calculate the ship's drag resistance value
        CalculateAirResistance();

        _prevDF = _DriveForce;
        _prevAOR = _AngleOfRoll;
        _prevHH = _HoverHeight;
        _prevTV = _TerminalVelocity;
        _prevMHH = _MaxGroundDist;
    }

    private void Update()
    {
        _AnimationManager.UpdateDrivingAnimation(Mathf.Abs(_Speed));

        if (_PlayerInput.IsJump && _IsOnGround)
        {
            _Rigidbody.velocity += transform.up * _JumpForce;
            _GameplaySFX.PlayJump();
        }

        if (_PlayerInput.IsNitrous)
        {
            _IsBoost = _NitrousSystem.Activate();

            if (_IsBoost)
            {
                _BoostEffectCooldown = 0.01f;
            }
        }

        if (_PlayerInput.IsNitrousOpen)
        {
            _GameplaySFX.PlayNOSOpen();
        }

        if (_PlayerInput.IsNitrousOff)
        {
            _GameplaySFX.PlayNOSOff();
        }

        if (_IsBoost)
        {
            _PPController.EnableBoostEffect();
            _BoostEffectCooldown -= Time.deltaTime;

            if (_BoostEffectCooldown <= 0.0f)
            {
                _IsBoost = false;
            }
        }
        else
        {
            _PPController.DisableBoostEffect();
        }

        if (_IsOnGround == false)
        {
            _InAirDuration += Time.deltaTime;
        }

        if (_IsOnGround && _InAirDuration >= 1.0f)
        {
            _GameplaySFX.PlayLand();
            _InAirDuration = 0.0f;
        }

        var position = PositionManager.Instance.GetCurrentPosition(photonView.Owner.ActorNumber);

        switch (position)
        {
            case 1:
                RubberBanding(_FirstRB.DriveForce, _FirstRB.MaxSpeed, _FirstRB.AngleOfRoll);
                break;
            case 2:
                RubberBanding(_SecondRB.DriveForce, _SecondRB.MaxSpeed, _SecondRB.AngleOfRoll);
                break;
            case 3:
                RubberBanding(_ThirdRB.DriveForce, _ThirdRB.MaxSpeed, _ThirdRB.AngleOfRoll);
                break;
            case 4:
                RubberBanding(_FourthRB.DriveForce, _FourthRB.MaxSpeed, _FourthRB.AngleOfRoll);
                break;
        }
    }

    public void BoostEffect()
    {
        _IsBoost = true;
    }

    private void FixedUpdate()
    {
        // Calculate the current speed by using the dot product
        CalculateSpeed();

        // Calculate the forces to be applied to the ship
        CalculatHover();
        CalculatePropulsion();
    }

    public void ActivateZeroBolt()
    {
        _IsZeroBolt = true;
        _ZeroBoltDuration = _BoltDuration;
    }

    private void RubberBanding(float df, float ms, float aor)
    {
        if (_PlayerInput.IsNitrous)
        {
            ms += 20;
        }

        if (_IsZeroBolt)
        {
            _DriveForce = Mathf.Lerp(_DriveForce, df / 2, Time.deltaTime);

            _ZeroBoltDuration -= Time.deltaTime;
            if (_ZeroBoltDuration <= 0.0f)
            {
                _IsZeroBolt = false;
            }
        }
        else
        {
            _DriveForce = Mathf.Lerp(_DriveForce, df, Time.deltaTime);
        }
        _TerminalVelocity = Mathf.Lerp(_TerminalVelocity, ms, Time.deltaTime);
        _AngleOfRoll = Mathf.Lerp(_AngleOfRoll, aor, Time.deltaTime);
    }

    private void CalculatHover()
    {
        //This variable will hold the "normal" of the ground. Think of it as a line
        //the points "up" from the surface of the ground
        Vector3 groundNormal;

        //Calculate a ray that points straight down from the ship
        Ray ray = new Ray(transform.position, -transform.up);

        //Declare a variable that will hold the result of a raycast
        RaycastHit hitInfo;

        //Determine if the ship is on the ground by Raycasting down and seeing if it hits 
        //any collider on the whatIsGround layer
        Physics.Raycast(ray, out hitInfo, _MaxGroundDist, _GroundLayer);

        Debug.DrawLine(transform.position, transform.position + (-transform.up * _MaxGroundDist), Color.red);

        if (hitInfo.distance <= _MaxGroundDist && hitInfo.distance > 0)
        {
            _IsOnGround = true;
        }
        else
        {
            _IsOnGround = false;
        }

        //If the ship is on the ground...
        if (_IsOnGround)
        {
            //...determine how high off the ground it is...
            float height = hitInfo.distance;
            //...save the normal of the ground...
            groundNormal = hitInfo.normal.normalized;
            //...use the PID controller to determine the amount of hover force needed...
            float forcePercent = _HoverPID.Seek(_HoverHeight, height);

            //...calulcate the total amount of hover force based on normal (or "up") of the ground...
            Vector3 force = groundNormal * _HoverForce * forcePercent;
            //...calculate the force and direction of gravity to adhere the ship to the 
            //track (which is not always straight down in the world)...
            Vector3 gravity = -groundNormal * _HoverGravity * height;

            //...and finally apply the hover and gravity forces
            _Rigidbody.AddForce(force, ForceMode.Acceleration);
            _Rigidbody.AddForce(gravity, ForceMode.Acceleration);
        }
        //...Otherwise...
        else
        {
            //...use Up to represent the "ground normal". This will cause our ship to
            //self-right itself in a case where it flips over
            groundNormal = Vector3.up;

            //Calculate and apply the stronger falling gravity straight down on the ship
            Vector3 gravity = -groundNormal * _FallGravity;
            _Rigidbody.AddForce(gravity, ForceMode.Acceleration);
        }

        //Calculate the amount of pitch and roll the ship needs to match its orientation
        //with that of the ground. This is done by creating a projection and then calculating
        //the rotation needed to face that projection
        Vector3 projection = Vector3.ProjectOnPlane(transform.forward, groundNormal);
        Quaternion rotation = Quaternion.LookRotation(projection, groundNormal);

        //Move the ship over time to match the desired rotation to match the ground. This is 
        //done smoothly (using Lerp) to make it feel more realistic
        _Rigidbody.MoveRotation(Quaternion.Lerp(_Rigidbody.rotation, rotation, Time.deltaTime * 10f));

        //Calculate the angle we want the ship's body to bank into a turn based on the current rudder.
        //It is worth noting that these next few steps are completetly optional and are cosmetic.
        //It just feels so darn cool
        _Angle = _AngleOfRoll * -_PlayerInput.Rudder * 1.5f;

        //Calculate the rotation needed for this new angle
        Quaternion bodyRotation = transform.rotation * Quaternion.Euler(0f, 0f, _Angle);
        //Finally, apply this angle to the ship's body
        _VehicleBody.rotation = Quaternion.Lerp(_VehicleBody.rotation, bodyRotation, Time.deltaTime * 10f);
    }

    private void CalculatePropulsion()
    {
        //Calculate the yaw torque based on the rudder and current angular velocity
        float rotationTorque = _PlayerInput.Rudder - _Rigidbody.angularVelocity.y;
        //Apply the torque to the ship's Y axis
        _Rigidbody.AddRelativeTorque(0f, rotationTorque, 0f, ForceMode.VelocityChange);

        //Calculate the current sideways speed by using the dot product. This tells us
        //how much of the ship's velocity is in the "right" or "left" direction
        float sidewaysSpeed = Vector3.Dot(_Rigidbody.velocity, transform.right);

        //Calculate the desired amount of friction to apply to the side of the vehicle. This
        //is what keeps the ship from drifting into the walls during turns. If you want to add
        //drifting to the game, divide Time.fixedDeltaTime by some amount
        Vector3 sideFriction = -transform.right * (sidewaysSpeed / Time.fixedDeltaTime);

        //Finally, apply the sideways friction
        _Rigidbody.AddForce(sideFriction, ForceMode.Acceleration);

        //If not propelling the ship, slow the ships velocity
        if (_PlayerInput.Thruster <= 0f)
        {
            _Rigidbody.velocity *= _SlowingVelFactor;
        }

        //Braking or driving requires being on the ground, so if the ship
        //isn't on the ground, exit this method
        // if (_IsOnGround == false)
        //     return;

        //If the ship is braking, apply the braking velocty reduction
        if (_PlayerInput.IsBraking && _IsOnGround)
        {
            _Rigidbody.velocity *= _BrakingVelFactor;
            return;
        }

        //Calculate and apply the amount of propulsion force by multiplying the drive force
        //by the amount of applied thruster and subtracting the drag amount
        float propulsion = _DriveForce * _PlayerInput.Thruster - drag * Mathf.Clamp(_Speed, 0f, _TerminalVelocity);
        _Rigidbody.AddForce(transform.forward * propulsion, ForceMode.Acceleration);

        _Rigidbody.velocity = Vector3.ClampMagnitude(_Rigidbody.velocity, _TerminalVelocity);
    }


    private void CalculateSpeed()
    {
        _Speed = Vector3.Dot(_Rigidbody.velocity, transform.forward);
    }

    private void CalculateAirResistance()
    {
        drag = (_DriveForce / _TerminalVelocity) * 0.10f;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Wall"))
        {
            Debug.Log("Enter");
            // Calculate angle between the collision point and the player
            Vector3 direction = other.contacts[0].point - transform.position;

            // Get the opposite Vector3 and normalize it
            direction = -direction.normalized;

            direction = new Vector3(direction.x, 0.0f, direction.z);

            // Add force in the direction of the contact point and multiply it by force
            GetComponent<Rigidbody>().AddForce(direction * 50.0f, ForceMode.Impulse);
            //...calculate how much upward impulse is generated and then push the vehicle down by that amount 
            //to keep it stuck on the track (instead up popping up over the wall)
            Vector3 upwardForceFromCollision = Vector3.Dot(other.impulse, transform.up) * transform.up;
            _Rigidbody.AddForce(-upwardForceFromCollision, ForceMode.Impulse);

            _GameplaySFX.PlayImpact();
        }
    }

    public float GetSpeedPercentage()
    {
        //Returns the total percentage of speed the ship is traveling
        return _Rigidbody.velocity.magnitude / _TerminalVelocity;
    }

    public float GetAnglePercentage()
    {
        return (_Angle / _AngleOfRoll) * 100;
    }

    public static void RefreshInstance(ref VehicleMovement vehicle, VehicleMovement prefab)
    {
        var position = new Vector3(1390.0f, 60.0f, 1245.0f);
        //var position = new Vector3(0.0f, 3.0f, 0.0f);
        var rotation = Quaternion.identity;

        if (vehicle != null)
        {
            position = vehicle.transform.position;
            rotation = vehicle.transform.rotation;
            PhotonNetwork.Destroy(vehicle.gameObject);
        }

        vehicle = PhotonNetwork.Instantiate(prefab.gameObject.name, position, rotation).GetComponent<VehicleMovement>();
        vehicle.transform.SetParent(GameObject.FindGameObjectWithTag("PlayerList").transform);
        vehicle.name = PhotonNetwork.NickName + " Zero Chaser ALPHA";
    }

    public void Respawn()
    {
        var position = new Vector3(1390.0f, 60.0f, 1245.0f);
        //var position = new Vector3(0.0f, 3.0f, 0.0f);
        var rotation = Quaternion.identity;

        this.transform.position = position;
        this.transform.rotation = rotation;
    }

    public void Respawn(Vector3 pos, Quaternion rot)
    {
        this.transform.position = pos;
        this.transform.rotation = rot;

        _Rigidbody.velocity = Vector3.zero;
    }

    public void ResetSettings()
    {
        _DriveForce = _prevDF;
        _AngleOfRoll = _prevAOR;
        _HoverHeight = _prevHH;
        _TerminalVelocity = _prevTV;
        _MaxGroundDist = _prevMHH;
    }

    public void SetDriveForce(float value)
    {
        _DriveForce += value;
    }

    public void SetHoverHeight(float value)
    {
        _HoverHeight += value;
    }

    public void SetAngleOfRoll(float value)
    {
        _AngleOfRoll += value;
    }

    public void SetMaxSpeed(float value)
    {
        _TerminalVelocity += value;
    }

    public void SetMaxHoverHeight(float value)
    {
        _MaxGroundDist += value;
    }

    public float GetDriveForce()
    {
        return _DriveForce;
    }

    public float GetHoverHeight()
    {
        return _HoverHeight;
    }

    public float GetAngleOfRoll()
    {
        return _AngleOfRoll;
    }

    public float GetSpeed()
    {
        return _Rigidbody.velocity.magnitude;
    }

    public float GetMaxSpeed()
    {
        return _TerminalVelocity;
    }

    public float GetMaxHoverHeight()
    {
        return _MaxGroundDist;
    }
}