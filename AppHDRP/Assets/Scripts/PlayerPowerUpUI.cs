﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPowerUpUI : MonoBehaviour
{
    [SerializeField] private List<Sprite> _PowerUpIcons;

    [SerializeField] private Image _Image;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void UpdateImage(int index)
    {
        gameObject.SetActive(true);
        _Image.sprite = _PowerUpIcons[index];
    }

    public void UpdateImage(PowerUpType type)
    {
        if(type == PowerUpType.NONE)
        {
            gameObject.SetActive(false);
        }
    }
}
