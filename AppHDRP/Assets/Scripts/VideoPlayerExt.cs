﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerExt : MonoBehaviour
{
    [SerializeField] private RawImage _RawImage;
    private VideoPlayer _VideoPlayer;

    private void Start()
    {
        _VideoPlayer = GetComponent<VideoPlayer>();
        _VideoPlayer.Prepare();
    }

    public IEnumerator PlayVideo()
    {
        while (_VideoPlayer.isPrepared == false)
        {
            yield return new WaitForSeconds(1.0f);
            break;
        }

        _RawImage.texture = _VideoPlayer.texture;
        _VideoPlayer.Play();
    }

    public void ResetVideo()
    {
        _VideoPlayer.Stop();
        _VideoPlayer.Prepare();
    }
}
