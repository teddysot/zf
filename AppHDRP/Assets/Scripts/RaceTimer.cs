﻿using UnityEngine;
using Photon.Pun;
using System;

public class RaceTimer : MonoBehaviourPunCallbacks
{
    private RaceTimerUI _RaceTimerUI;

    public delegate void RaceTimerHasExpired();

    public static event RaceTimerHasExpired OnRaceTimerHasExpired;

    private bool _IsTimerRunning;

    private int _StartTime = 0;

    private void Awake()
    {
        _RaceTimerUI = GetComponent<RaceTimerUI>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_IsTimerRunning == false)
        {
            return;
        }

        if (_StartTime == 0)
        {
            return;
        }

        var playerEndtime = (int)PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Timespan];

        if (playerEndtime != 0)
        {
            Debug.Log(playerEndtime);
            this.enabled = false;
        }

        _RaceTimerUI.UpdateGameTime(_StartTime);
    }

    // Start is called before the first frame update
    public void RaceStart()
    {
        GameManager.Instance.RaceStart();

        _StartTime = GameManager.Instance.RaceStartTime;

        _IsTimerRunning = true;
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient == false)
        {
            object raceTime;

            if (propertiesThatChanged.TryGetValue(CustomProperties.StartTime, out raceTime))
            {
                _StartTime = (int)raceTime;

                GameManager.Instance.RaceStartTime = _StartTime;

                Debug.Log($"[RaceTimer] {PhotonNetwork.LocalPlayer.NickName} RaceTime2 : {raceTime}");
            }
        }
    }
}
