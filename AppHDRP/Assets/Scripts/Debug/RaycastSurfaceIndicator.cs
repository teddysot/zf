﻿using UnityEngine;

public class RaycastSurfaceIndicator : MonoBehaviour
{
    private void Update()
    {
        if(Physics.Raycast(transform.position, -transform.up, out var hit, 50f))
        {
            Debug.DrawRay(hit.point, transform.up * 0.5f, Color.blue, 4f);
            Debug.DrawRay(hit.point, hit.normal * 0.25f, Color.red, 4f);
        }
    }
}
