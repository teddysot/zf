﻿using UnityEngine;
using Photon.Pun;

public class Attack : MonoBehaviour
{
    private SFXPlayer _SFXPlayer;
    private CombatType _CombatType;
    private PlayerManager _Player;

    private void Awake() 
    {
        _SFXPlayer = GetComponent<SFXPlayer>();
        _Player = GetComponent<PlayerManager>();
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.GetInstanceID() == gameObject.GetInstanceID())
        {
            return;
        }
        
        if(other.CompareTag("Player"))
        {
            _SFXPlayer.PlaySwordBike(gameObject);
            //ScoreManager.Instance.AddScore(ref _Player, _CombatType);
            other.GetComponent<Analytics>().HitCollect();
        }
    }

    public void SetATKType(CombatType type)
    {
        Debug.Log($"[Attack] {GetComponent<PhotonView>().Owner.NickName} set attack type: {type}");
        _CombatType = type;
    }
}
