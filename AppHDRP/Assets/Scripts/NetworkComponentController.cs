using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class NetworkComponentController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject[] _ObjectsToDelete;

    [SerializeField]
    private Component[] _ComponentsToRemove;

    private void Awake()
    {
        if (photonView.IsMine == false)
        {
            GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
            foreach (var item in _ObjectsToDelete)
            {
                Destroy(item);
            }
            foreach (var item in _ComponentsToRemove)
            {
                Destroy(item);
            }
        }
    }
}