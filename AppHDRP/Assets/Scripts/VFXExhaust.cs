﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class VFXExhaust : MonoBehaviour
{
    [SerializeField] private VisualEffect _VFX;
    private string _IdleName = "IsIdle";
    private string _BoostName = "IsBoost";
    private string _NitrousName = "IsNitrous";

    [SerializeField] private float _BoostCooldown = 2.0f;
    private bool _IsBoost = false;

    private VehicleMovement _VehicleMovement;
    private PlayerInput _PlayerInput;
    private NitrousSystem _NitrousSystem;

    private void Awake()
    {
        _VehicleMovement = GetComponent<VehicleMovement>();
        _PlayerInput = GetComponent<PlayerInput>();
        _NitrousSystem = GetComponent<NitrousSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_VehicleMovement.GetSpeed() > 1.0f)
        {
            _VFX.SetBool(_IdleName, false);
        }
        else
        {
            _VFX.SetBool(_IdleName, true);
        }

        if (_IsBoost)
        {
            _VFX.SetBool(_BoostName, true);
        }
        else
        {
            _VFX.SetBool(_BoostName, false);
        }

        if (_PlayerInput.IsNitrous && _NitrousSystem.CurrentAmount > 0.0f)
        {
            _VFX.SetBool(_NitrousName, true);
        }
        else
        {
            _VFX.SetBool(_NitrousName, false);
        }

        BoostCooldown();
    }

    private float _Duration;
    private void BoostCooldown()
    {
        if (_IsBoost == false)
        {
            return;
        }

        _Duration -= Time.deltaTime;

        if (_Duration <= 0.0f)
        {
            _IsBoost = false;
        }
    }

    public void ActivateBoost()
    {
        _IsBoost = true;
        _Duration = _BoostCooldown;
    }
}
