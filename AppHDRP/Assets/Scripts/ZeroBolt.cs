﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class ZeroBolt : MonoBehaviour
{
    [SerializeField] private float _LifeTime = 3.0f;

    void Update()
    {
        if (_LifeTime <= 0.0f)
        {
            Destroy(this.gameObject);
            return;
        }

        _LifeTime -= Time.deltaTime;
    }
}
