﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRightAnimation : StateMachineBehaviour
{
    private AnimationManager _AnimationManager;
    private BoxCollider _SwordCollider;
    private SFXPlayer _SFXPlayer;

    private bool _IsSFXPlayed = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var atk = animator.gameObject.GetComponent<Attack>();

        atk.SetATKType(CombatType.REGULAR);

        _AnimationManager = animator.gameObject.GetComponent<AnimationManager>();

        _SFXPlayer = animator.gameObject.GetComponent<SFXPlayer>();

        _SwordCollider = _AnimationManager.SwordRight.GetComponentInChildren<BoxCollider>();

        _SwordCollider.enabled = true;

        _IsSFXPlayed = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime >= 0.05f && _IsSFXPlayed == false)
        {
            _SFXPlayer.PlaySwordSwing(animator.gameObject);
            _IsSFXPlayed = true;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _SwordCollider.enabled = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
