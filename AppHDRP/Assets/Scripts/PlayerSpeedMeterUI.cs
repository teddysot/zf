﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerSpeedMeterUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _Text;

    public void UpdateSpeed(float speed)
    {
        var roundSpeed = Mathf.Floor(speed * 1.56f);
        var absSpeed = Mathf.Abs(roundSpeed);
        _Text.text = absSpeed.ToString();
    }
}
