﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RespawnEffect : MonoBehaviour
{
    [SerializeField] private Renderer _Renderer;
    [SerializeField] private Material _BikeMaterial;
    [SerializeField] private Material _RespawnMaterial;
    [SerializeField] private float _FadeDuration = 1.5f;


    public void Activate()
    {
        _Renderer.material = _RespawnMaterial;
        _Renderer.material.DOFloat(3.0f, "_AlphaIntensity", _FadeDuration).OnComplete(() =>
        {
            _Renderer.material = _BikeMaterial;
        });
    }
}
