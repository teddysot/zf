﻿using Photon.Pun;
using UnityEngine;

public class IonCannon : MonoBehaviourPunCallbacks
{
    [SerializeField] private float _LifeTime = 3.0f;

    // Update is called once per frame
    void Update()
    {
        if (_LifeTime <= 0.0f)
        {
            Destroy(this.gameObject);
            return;
        }

        _LifeTime -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Debug.Log($"{other.GetComponent<PhotonView>().Owner.NickName} got hit by Ion Cannon");
            //other.GetComponent<Rigidbody>().velocity *= 0.00f;
            //other.GetComponent<PlayerPowerUpController>().IonCannonHit();
        }
    }
}
