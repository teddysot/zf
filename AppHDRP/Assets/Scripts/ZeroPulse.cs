﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class ZeroPulse : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject _ZeroPulse;
    [SerializeField] private float _LifeTime = 3.0f;
    [SerializeField] private float _PlayRateMultiply = 5.0f;

    private bool _IsActivate = false;

    private VisualEffect _VFX;
    private SphereCollider _Collider;

    // Start is called before the first frame update
    void Start()
    {
        _VFX = _ZeroPulse.GetComponent<VisualEffect>();
        _Collider = GetComponent<SphereCollider>();

        _VFX.playRate = _PlayRateMultiply;
    }

    // Update is called once per frame
    void Update()
    {
        if (_LifeTime <= 0.0f)
        {
            Destroy(this.gameObject);
            return;
        }

        _LifeTime -= Time.deltaTime;
        _Collider.radius += Time.deltaTime * 2.5f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //var player = NetworkManager.Instance.LocalPlayer.GetComponent<PlayerManager>();
            //ScoreManager.Instance.AddScore(ref player, PowerUpType.ZEROPULSE);
            //other.GetComponent<Rigidbody>().velocity *= 0.60f;
            //other.GetComponent<PlayerManager>().ActivateZeroPulse();
            //other.GetComponent<PlayerPowerUpController>().ZeroPulseHit();
            //Debug.Log($"{other.GetComponent<PhotonView>().Owner.NickName} got hit by ZeroPulse");
        }
    }
}
