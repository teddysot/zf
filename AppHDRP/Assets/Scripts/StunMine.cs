﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class StunMine : MonoBehaviourPunCallbacks
{
    [SerializeField] private LayerMask _GroundLayer;

    private Rigidbody _RB;
    private float _MaxGroundDist = 0.2f;
    private string _StunMineEvent = "Play_SFX_PUP_StunMine";

    private void Awake()
    {
        _RB = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        AkSoundEngine.PostEvent(_StunMineEvent, this.gameObject);
    }

    private void Update()
    {
        Debug.DrawLine(transform.position, transform.position + (-transform.up * 5), Color.red);

        if (_RB.isKinematic)
        {
            return;
        }

        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hitInfo;

        Physics.Raycast(ray, out hitInfo, 5, _GroundLayer);

        if (hitInfo.distance <= _MaxGroundDist)
        {
            _RB.isKinematic = true;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
