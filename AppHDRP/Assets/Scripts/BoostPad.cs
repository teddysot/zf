﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class BoostPad : MonoBehaviour
{
    [SerializeField] private float velocity = 100.0f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<GameplaySFX>().PlayPUP(3);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<VehicleMovement>().BoostEffect();

            var position = PositionManager.Instance.GetCurrentPosition(other.GetComponent<PhotonView>().Owner.ActorNumber);

            switch (position)
            {
                case 1:
                    velocity = 200.0f;
                    break;
                case 2:
                    velocity = 210.0f;
                    break;
                case 3:
                    velocity = 220.0f;
                    break;
                case 4:
                    velocity = 230.0f;
                    break;
            }
            other.GetComponent<Rigidbody>().velocity = other.transform.forward * velocity;
            var vfxv1 = other.GetComponent<VFXExhaust>();
            var vfxv2 = other.GetComponent<VFXExhaustV2>();

            if (vfxv1 != null)
            {
                vfxv1.ActivateBoost();
            }

            if (vfxv2 != null)
            {
                vfxv2.ActivateBoost();
            }
        }
    }
}
