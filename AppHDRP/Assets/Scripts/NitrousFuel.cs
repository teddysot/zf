﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NitrousFuel : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<NitrousSystem>().FillNitrous(20.0f);
            Destroy(gameObject);
        }
    }
}
