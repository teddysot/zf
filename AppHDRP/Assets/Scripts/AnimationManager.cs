using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    [SerializeField] private GameObject _SwordLeft;
    [SerializeField] private GameObject _SwordRight;

    public GameObject SwordLeft => _SwordLeft;
    public GameObject SwordRight => _SwordRight;

    private Animator _Animator;

    private void Start()
    {
        _Animator = GetComponent<Animator>();
    }

    public void UpdateAttackAnimation(bool attackRight, bool attackLeft, bool attackBoth)
    {
        if (attackBoth == false)
        {
            _Animator.SetBool("AttackRight", attackRight);
            _Animator.SetBool("AttackLeft", attackLeft);
        }

        _Animator.SetBool("AttackBoth", attackBoth);
    }

    public void UpdateDrivingAnimation(float speed)
    {
        _Animator.SetFloat("Speed", speed);
    }

    public void UpdateStunAnimation()
    {
        _Animator.SetTrigger("Stun");
    }
}