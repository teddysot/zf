﻿using UnityEngine;
using UnityEngine.Playables;

public class CinematicController : MonoBehaviour
{
    [SerializeField] private PlayableDirector _RaceStartTimeline;
    [SerializeField] private GameObject _VCam1;
    [SerializeField] private GameObject _VCam2;

    private bool _IsPlay = false;

    public void PlayRaceStart()
    {
        _VCam1.SetActive(true);
        _RaceStartTimeline.Play();
    }

    private void Update()
    {
        if (_IsPlay)
        {
            return;
        }

        if (_RaceStartTimeline.time >= 3.0f)
        {
            _VCam1.SetActive(false);
            _VCam2.SetActive(true);
            _IsPlay = true;
        }
    }

    public void StopRaceStart()
    {
        _VCam1.SetActive(false);
        _VCam2.SetActive(false);
        _RaceStartTimeline.Stop();
    }
}
