﻿using TMPro;
using UnityEngine;
using Photon.Pun;

public class PlayerScoreUI : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI _PlayerScoreText;

    private int _CurrentScore = 0;
    private int _PrevScore = 0;

    float _Lerp = 0.0f;
    float _Duration = 2.0f;

    private void LateUpdate()
    {
        LerpScore();
    }

    private void LerpScore()
    {
        if (_PrevScore != _CurrentScore)
        {
            _Lerp += Time.deltaTime / _Duration;

            _PrevScore = (int)Mathf.Lerp(_PrevScore, _CurrentScore, _Lerp);
            _PlayerScoreText.text = _PrevScore.ToString();
        }
    }

    public void UpdateScore()
    {
        _Lerp = 0.0f;
        //_Duration = score / 1000;
        _CurrentScore = (int)PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Score];
    }
}
