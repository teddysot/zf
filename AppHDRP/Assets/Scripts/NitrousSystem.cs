﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NitrousSystem : MonoBehaviourPunCallbacks
{
    [SerializeField] private float _MaxCapacity = 100.0f;
    [SerializeField] private float _CurrentAmount;
    [SerializeField] private float _SpeedMultiply = 2.0f;
    private float _EnergyUsageMultiply = 20.0f;
    private GameplaySFX _GameplaySFX;

    [SerializeField] private float _SFXCooldown = 2.0f;
    private float _Duration;
    private bool _IsPlayed = false;

    [SerializeField] private float _RegenCooldown = 2.0f;
    private float _RegenDur;
    private bool _IsRegen = false;

    public float CurrentAmount { get { return _CurrentAmount; } private set { _CurrentAmount = value; } }

    void Start()
    {
        _CurrentAmount = _MaxCapacity;
        GetComponent<PlayerManager>().UpdateNitrousBar(_CurrentAmount);
        _GameplaySFX = GetComponent<GameplaySFX>();
        _Duration = _SFXCooldown;
        _RegenDur = _RegenCooldown;
    }

    private void Update()
    {
        _RegenDur -= Time.deltaTime;

        if (_RegenDur <= 0.0f)
        {
            _IsRegen = true;
            _RegenDur = _RegenCooldown;
        }

        if (_IsRegen)
        {
            var position = PositionManager.Instance.GetCurrentPosition(photonView.Owner.ActorNumber);

            switch (position)
            {
                case 1:
                    FillRegen(5.0f);
                    break;
                case 2:
                    FillRegen(7.0f);
                    break;
                case 3:
                    FillRegen(10.0f);
                    break;
                case 4:
                    FillRegen(15.0f);
                    break;
            }

            _IsRegen = false;
        }

        if (_IsPlayed)
        {
            _Duration -= Time.deltaTime;
            if (_Duration <= 0.0f)
            {
                _IsPlayed = false;
                _Duration = _SFXCooldown;
            }
        }
    }

    public bool Activate()
    {
        if (_CurrentAmount <= 0.0f)
        {
            _CurrentAmount = 0.0f;

            if (_IsPlayed == false)
            {
                _GameplaySFX.PlayPUP(1);
                Debug.Log("[NitrousSystem] Fuel is empty!");
                _IsPlayed = true;
            }
            return false;
        }


        var position = PositionManager.Instance.GetCurrentPosition(photonView.Owner.ActorNumber);

        switch (position)
        {
            case 1:
                _CurrentAmount -= Time.deltaTime * _EnergyUsageMultiply;
                break;
            case 2:
                _CurrentAmount -= Time.deltaTime * (_EnergyUsageMultiply * 0.90f);
                break;
            case 3:
                _CurrentAmount -= Time.deltaTime * (_EnergyUsageMultiply * 0.75f);
                break;
            case 4:
                _CurrentAmount -= Time.deltaTime * (_EnergyUsageMultiply * 0.60f);
                break;
        }

        GetComponent<Rigidbody>().velocity += transform.forward * (1 + Time.fixedDeltaTime);
        GetComponent<PlayerManager>().UpdateNitrousBar(_CurrentAmount);
        _GameplaySFX.PlayNOSBurn();

        return true;
    }

    public void FillNitrous(float value)
    {
        float prevAmount = _CurrentAmount;

        _CurrentAmount += value;

        if (prevAmount < 100.0f)
        {
            _GameplaySFX.PlayPUP(6);
        }
        else
        {
            if (_CurrentAmount > 100.0f)
            {
                _CurrentAmount = 100.0f;
                _GameplaySFX.PlayPUP(2);
            }
        }

        GetComponent<PlayerManager>().UpdateNitrousBar(_CurrentAmount);
    }

    public void FillRegen(float value)
    {
        _CurrentAmount += value;

        if (_CurrentAmount > 100.0f)
        {
            _CurrentAmount = 100.0f;
        }

        GetComponent<PlayerManager>().UpdateNitrousBar(_CurrentAmount);
    }

    public void ResetNitrous()
    {
        _CurrentAmount = 100.0f;
        GetComponent<PlayerManager>().UpdateNitrousBar(_CurrentAmount);
    }
}
