using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager Instance;

    [SerializeField] private List<GameObject> _Prefab;
    [SerializeField] private GameObject _LocalPlayer;

    public GameObject LocalPlayer { get { return _LocalPlayer; } private set { _LocalPlayer = value; } }

    [SerializeField] private List<Transform> _SpawnPositions;
    [SerializeField] private List<Transform> _WaitingSpawnPositions;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        if (PhotonNetwork.IsConnected == false)
        {
            SceneManager.LoadScene("SCE_MainMenu_02");
            return;
        }
    }

    private void Start()
    {
        WaitingAreaSpawn();
    }

    private void Update()
    {
        if (PhotonNetwork.IsConnected == false)
        {
            AkSoundEngine.SetState("GameState", "FrontEnd");
            SceneManager.LoadScene("SCE_MainMenu_02");
            return;
        }
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        Debug.Log($"[NetworkManager] Player: {newPlayer.NickName} joined the room");
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        base.OnPlayerEnteredRoom(otherPlayer);

        Debug.Log($"[NetworkManager] Player: {otherPlayer.NickName} exited the room");
    }

    public void WaitingAreaSpawn()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.ActorNumber);

        int numPlayer = PhotonNetwork.LocalPlayer.ActorNumber;

        Vector3 position = SpawnPosition(numPlayer, 0);
        Quaternion rotation = SpawnRotation(numPlayer, 0);

        var numBike = (int)PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Bike];
        var vehicle = PhotonNetwork.Instantiate(_Prefab[numBike].name, position, rotation);
        vehicle.GetComponent<PlayerManager>().SetCheckpoint(position, rotation, 0);
        vehicle.transform.SetParent(GameObject.FindGameObjectWithTag("PlayerList").transform);
        vehicle.name = PhotonNetwork.NickName + " Zero Chaser ALPHA";
        vehicle.GetComponent<Rigidbody>().velocity *= 0.0f;

        _LocalPlayer = vehicle;
    }

    public void Spawn()
    {
        Debug.Log(_LocalPlayer.GetComponent<PhotonView>().Owner.ActorNumber);

        int numPlayer = _LocalPlayer.GetComponent<PhotonView>().Owner.ActorNumber;

        Vector3 position = SpawnPosition(numPlayer, 1);
        Quaternion rotation = SpawnRotation(numPlayer, 1);

        _LocalPlayer.GetComponent<PlayerManager>().SetCheckpoint(position, rotation, 0);
        _LocalPlayer.transform.position = position;
        _LocalPlayer.transform.rotation = rotation;
        _LocalPlayer.GetComponent<Rigidbody>().velocity *= 0.0f;
        _LocalPlayer.GetComponent<Rigidbody>().angularVelocity *= 0.0f;
        _LocalPlayer.GetComponent<PlayerManager>().ClearPowerUp();
        //_LocalPlayer.GetComponent<PlayerManager>().ClearScore();
        _LocalPlayer.GetComponent<NitrousSystem>().ResetNitrous();
        _LocalPlayer.GetComponent<PlayerInput>().ResetInput();
        _LocalPlayer.GetComponent<CinematicController>().PlayRaceStart();
    }

    private Vector3 SpawnPosition(int numPlayer, int type)
    {
        Vector3 position;
        if (type == 0)
        {
            switch (numPlayer)
            {
                case 1:
                    position = _WaitingSpawnPositions[0].position;
                    break;
                case 2:
                    position = _WaitingSpawnPositions[1].position;
                    break;
                case 3:
                    position = _WaitingSpawnPositions[2].position;
                    break;
                case 4:
                    position = _WaitingSpawnPositions[3].position;
                    break;
                default:
                    position = _WaitingSpawnPositions[0].position;
                    break;
            }
        }
        else
        {
            switch (numPlayer)
            {
                case 1:
                    position = _SpawnPositions[0].position;
                    break;
                case 2:
                    position = _SpawnPositions[1].position;
                    break;
                case 3:
                    position = _SpawnPositions[2].position;
                    break;
                case 4:
                    position = _SpawnPositions[3].position;
                    break;
                default:
                    position = _SpawnPositions[0].position;
                    break;
            }
        }

        return position;
    }

    private Quaternion SpawnRotation(int numPlayer, int type)
    {
        Quaternion rotation;

        if (type == 0)
        {
            switch (numPlayer)
            {
                case 1:
                    rotation = _WaitingSpawnPositions[0].rotation;
                    break;
                case 2:
                    rotation = _WaitingSpawnPositions[1].rotation;
                    break;
                case 3:
                    rotation = _WaitingSpawnPositions[2].rotation;
                    break;
                case 4:
                    rotation = _WaitingSpawnPositions[3].rotation;
                    break;
                default:
                    rotation = _WaitingSpawnPositions[0].rotation;
                    break;
            }
        }
        else
        {
            switch (numPlayer)
            {
                case 1:
                    rotation = _SpawnPositions[0].rotation;
                    break;
                case 2:
                    rotation = _SpawnPositions[1].rotation;
                    break;
                case 3:
                    rotation = _SpawnPositions[2].rotation;
                    break;
                case 4:
                    rotation = _SpawnPositions[3].rotation;
                    break;
                default:
                    rotation = _SpawnPositions[0].rotation;
                    break;
            }
        }

        return rotation;
    }
}