﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class RaceEndTimer : MonoBehaviourPunCallbacks
{
    [SerializeField] private Leaderboard _Leaderboard;

    private RaceTimerUI _RaceTimerUI;

    private RaceTimer _RaceTimer;

    public delegate void RaceEndTimerHasExpired();

    public static event RaceEndTimerHasExpired OnRaceEndTimerHasExpired;

    private bool _IsTimerRunning;

    private int _EndTime = 0;

    [Header("Countdown time in seconds")]
    [SerializeField] private int Countdown = 30;

    private void Awake()
    {
        _RaceTimerUI = GetComponent<RaceTimerUI>();
        _RaceTimer = GetComponent<RaceTimer>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_IsTimerRunning == false)
        {
            return;
        }

        if (_EndTime == 0)
        {
            return;
        }

        var timer = PhotonNetwork.ServerTimestamp - _EndTime;
        var countdown = Countdown - TimeSpan.FromMilliseconds(timer).Seconds;

        if (countdown <= 10)
        {
            _RaceTimerUI.UpdateEndTime(countdown);
        }

        if (countdown > 0)
        {
            return;
        }

        _IsTimerRunning = false;

        if (OnRaceEndTimerHasExpired != null)
        {
            OnRaceEndTimerHasExpired();
        }

        _RaceTimerUI.ClearTime();
        _Leaderboard.gameObject.SetActive(true);
        _Leaderboard.ShowBoard();
        this.enabled = false;
    }

    // Start is called before the first frame update
    public void RaceEnd()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            _EndTime = GameManager.Instance.RaceEndTime;
        }

        _IsTimerRunning = true;
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient == false)
        {
            object endTime;
            if (propertiesThatChanged.TryGetValue(CustomProperties.EndTime, out endTime))
            {
                _EndTime = (int)endTime;

                GameManager.Instance.RaceEndTime = _EndTime;

                Debug.Log($"[RaceTimer] {PhotonNetwork.LocalPlayer.NickName} EndTime : {endTime}");
            }
        }
    }
}
