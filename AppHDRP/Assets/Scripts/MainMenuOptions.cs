﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.HDPipeline;
using TMPro;

public class MainMenuOptions : MonoBehaviour
{
    [Header("Post Processing References")]
    [SerializeField] private Volume _Volume;

    [SerializeField] private TextMeshProUGUI _GammaValueText;

    private LiftGammaGain _LGG;

    // Start is called before the first frame update
    private void Start()
    {
        LiftGammaGain tmp;
        if (_Volume.profile.TryGet<LiftGammaGain>(out tmp))
        {
            _LGG = tmp;
            _LGG.gamma.value = PlayerInfo.GammaSetting;
        }
        else
        {
            Debug.LogError("[MainMenuOptions] Missing LiftGammaGain in Volume");
        }
    }

    public void OnGammaValueChanged(float value)
    {
        _LGG.gamma.value = new Vector4(value, value, value, value - 1.0f);

        if (value < 1.00f)
        {
            _GammaValueText.text = "0" + value.ToString(".0");
        }
        else
        {
            _GammaValueText.text = value.ToString(".0");
        }

        PlayerInfo.GammaSetting = _LGG.gamma.value;
    }
}
