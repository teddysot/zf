﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class CreateRoom : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_InputField _RoomNameInput;
    [SerializeField] private GameObject _AlertText;

    private int _RoomNameCount = 0;

    private string _TypeForwardEvent = "Play_TYP_Forward";
    private string _TypeBackwardEvent = "Play_TYP_Backward";

    public void OnCreateRoomClicked()
    {
        if (_RoomNameInput.text.Length < 3 || _RoomNameInput.text.Length > 12)
        {
            _RoomNameInput.text = "";
            _RoomNameCount = 0;
            AkSoundEngine.PostEvent("Play_UI_Fail", this.gameObject);
            _AlertText.SetActive(true);
            return;
        }
        PhotonNetwork.CreateRoom(_RoomNameInput.text, new RoomOptions
        {
            MaxPlayers = (byte)4
        },
        TypedLobby.Default);
    }

    public void OnRoomNameTyping(string input)
    {
        if (input.Length > _RoomNameCount)
        {
            AkSoundEngine.PostEvent(_TypeForwardEvent, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_TypeBackwardEvent, this.gameObject);
        }

        _RoomNameCount = input.Length;
    }
}
