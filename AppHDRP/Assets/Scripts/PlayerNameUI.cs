﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class PlayerNameUI : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI _PlayerNameText;
    [SerializeField] private Vector3 _ScreenOffset = new Vector3(0f, 30f, 0f);

    private GameObject _Target;

    private float _CharacterControllerHeight = 0f;
    private Transform _TargetTransform;
    private Vector3 _TargetPosition;

    private void Update()
    {
        if (_Target == null)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    private void LateUpdate()
    {
        if (this.transform.position.z > 0.0f && this.transform.position.x.IsBetween(0.0f, Screen.width) && this.transform.position.y.IsBetween(0.0f, Screen.height))
        {
            _PlayerNameText.gameObject.SetActive(true);
        }
        else
        {
            _PlayerNameText.gameObject.SetActive(false);
        }

        if (_TargetTransform != null)
        {
            _TargetPosition = _TargetTransform.position;
            _TargetPosition.y += _CharacterControllerHeight;
            this.transform.position = Camera.main.WorldToScreenPoint(_TargetPosition) + _ScreenOffset;
        }
    }

    public void SetTarget(GameObject target)
    {
        if (target == null)
        {
            Debug.Log("[PlayerUI] Couldn't find target");
            return;
        }
        // Cache references for efficiency
        _Target = target;
        if (_PlayerNameText != null)
        {
            _PlayerNameText.text = target.GetComponent<PhotonView>().Owner.NickName;
        }

        _TargetTransform = this._Target.GetComponent<Transform>();

        CharacterController CC = _Target.GetComponent<CharacterController>();

        if (CC != null)
        {
            _CharacterControllerHeight = CC.height;
        }
    }
}
