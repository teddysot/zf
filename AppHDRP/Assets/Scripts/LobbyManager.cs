﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private Button _BtnLoginMaster;
    [SerializeField] private Button _BtnConnectRoom;
    [SerializeField] private Button _BtnSetName;

    [SerializeField] private TMP_InputField _InputField;
    [SerializeField] private TextMeshProUGUI _WelcomeText;
    [SerializeField] private TextMeshProUGUI _NickNameText;
    [SerializeField] private TextMeshProUGUI _WarningText;

    [SerializeField] private bool _TryConnectToMaster = false;
    [SerializeField] private bool _TryConnectToRoom = false;
    [SerializeField] private bool _SetNickName = false;

    private string _GameplayScene = "SCE_Gameplay";
    private string _NickName = "";

    private void Start()
    {
        _SetNickName = false;
        _TryConnectToMaster = false;
        _TryConnectToRoom = false;
    }

    private void Update()
    {
        var showLoginBtn = PhotonNetwork.IsConnected == false && _TryConnectToMaster == false;
        var showConnectRoomBtn = PhotonNetwork.IsConnected == true && _TryConnectToMaster == false && _TryConnectToRoom == false;

        if (_SetNickName)
        {
            _BtnLoginMaster.gameObject.SetActive(showLoginBtn);
            _BtnConnectRoom.gameObject.SetActive(showConnectRoomBtn);
        }
    }

    public void OnBikeSelectionClicked(int value)
    {
        PlayerInfo.Bike = value;
    }

    public void OnSetNameClicked()
    {
        if (_InputField.text.Length > 12 || _InputField.text.Length < 3)
        {
            _WarningText.text = "Nickname must be 3 - 12 letters";
            _InputField.text = "";
            _WarningText.gameObject.SetActive(true);
            return;
        }

        _WarningText.gameObject.SetActive(false);
        _WelcomeText.gameObject.SetActive(true);
        _NickNameText.gameObject.SetActive(true);

        _BtnSetName.gameObject.SetActive(false);
        _InputField.transform.parent.gameObject.SetActive(false);

        _NickName = _InputField.text;
        _NickNameText.text = _NickName;

        _SetNickName = true;
    }

    public void OnLoginClicked()
    {
        PhotonNetwork.OfflineMode = false;              // true would fake the connection
        PhotonNetwork.AutomaticallySyncScene = true;    // call PhotonNetowork.LoadLevel()
        PhotonNetwork.GameVersion = "V1";               // only people with same version can play together

        if (_NickName == "")
        {
            PhotonNetwork.NickName = "NONAME";              // player name
        }
        else
        {
            PhotonNetwork.NickName = _NickName;
        }

        _TryConnectToMaster = true;
        //PhotonNetwork.ConnectToMaster(ip, port, appID); // manual connection
        PhotonNetwork.SendRate = 50;
        PhotonNetwork.SerializationRate = 25;
        PhotonNetwork.ConnectUsingSettings();           // automatic connection based on PhotonServerSettings
        PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Bike] = 0;
    }

    public void OnConnectRoomClicked()
    {
        if (PhotonNetwork.IsConnected == false)
        {
            return;
        }

        _TryConnectToRoom = true;
        // PhotonNetwork.CreateRoom("Room Name");    // Create a specific room  - Error: OnCreateRoomFailed
        PhotonNetwork.JoinOrCreateRoom("ZeroFriction", new RoomOptions { MaxPlayers = 20 }, TypedLobby.Default);      // Join a specific room    - Error: OnJoinRoomFailed
        // PhotonNetwork.JoinRandomRoom();              // Join a random room      - Error: OnJoinRandomRoomFailed
        PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Bike] = PlayerInfo.Bike;
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        Debug.Log($"Room Amount: {roomList.Count}");

        foreach (var room in roomList)
        {
            Debug.Log($"Room Name: {room.Name}");
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);

        _TryConnectToMaster = false;
        _TryConnectToRoom = false;

        Debug.Log($"[LobbyManager] Disconnected: {cause}");
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        _TryConnectToMaster = false;

        Debug.Log($"[LobbyManager] Player: {PhotonNetwork.NickName} connected to Master");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        _TryConnectToRoom = false;
        Debug.Log($"[LobbyManager] Master: {PhotonNetwork.IsMasterClient} | Players in Room: {PhotonNetwork.CurrentRoom.PlayerCount} | Room Name: {PhotonNetwork.CurrentRoom.Name}");
        SceneManager.LoadScene(_GameplayScene);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);

        // No room available
        // Create a room (null as a name means "does not matter")
        PhotonNetwork.CreateRoom("ZeroFriction", new RoomOptions
        {
            MaxPlayers = 20
        });
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);

        // No room available
        // Create a room (null as a name means "does not matter")
        PhotonNetwork.CreateRoom("ZeroFriction", new RoomOptions
        {
            MaxPlayers = 20
        });
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);

        Debug.Log($"[LobbyManager] Create Room Failed: {message}");
        _TryConnectToRoom = false;
    }
}
