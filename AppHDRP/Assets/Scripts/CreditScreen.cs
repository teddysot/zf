﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditScreen : MonoBehaviour
{
    [SerializeField] private VideoPlayerExt _VideoPlayer;

    public void PlayVideo()
    {
        StartCoroutine(_VideoPlayer.PlayVideo());
    }
}
