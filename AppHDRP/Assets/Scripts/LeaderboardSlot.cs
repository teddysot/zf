﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LeaderboardSlot : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _Position;
    [SerializeField] private Image _BikeImage;
    [SerializeField] private TextMeshProUGUI _PlayerName;
    [SerializeField] private TextMeshProUGUI _Time;

    public void UpdatePlayerInfo(int pos, Sprite bike, string name, TimeSpan timespan)
    {
        switch (pos)
        {
            case 1:
                _Position.text = "1st";
                break;
            case 2:
                _Position.text = "2nd";
                break;
            case 3:
                _Position.text = "3rd";
                break;
            case 4:
                _Position.text = "4th";
                break;
        }

        _BikeImage.sprite = bike;
        _PlayerName.text = name;
        _Time.text = string.Format("{0:D2}:{1:D2}:{2:D3}", timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
    }
}
