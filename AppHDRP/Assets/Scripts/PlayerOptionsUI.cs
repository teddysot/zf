﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.HDPipeline;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerOptionsUI : MonoBehaviourPunCallbacks
{
    [SerializeField] private Slider _GammaSlider;
    [SerializeField] private GameObject _Controller;
    [SerializeField] private GameObject _Keyboard;
    private GameObject _PostProcessingGO;
    private Volume _Volume;
    private LiftGammaGain _LGG;

    private void Awake()
    {
        //var HUD = GameObject.FindGameObjectWithTag("HUD");
        //transform.SetParent(HUD.GetComponent<Transform>(), false);

        _PostProcessingGO = GameObject.FindGameObjectWithTag("PostProcessing");
        _Volume = _PostProcessingGO.GetComponent<Volume>();
    }

    // Start is called before the first frame update
    void Start()
    {
        LiftGammaGain tmp;
        if (_Volume.profile.TryGet<LiftGammaGain>(out tmp))
        {
            _LGG = tmp;
            _GammaSlider.value = PlayerInfo.GammaSetting.x;
            _LGG.gamma.value = PlayerInfo.GammaSetting;
        }
        else
        {
            Debug.LogError("[PlayerOptionsUI] Missing LiftGammaGain in Volume");
        }
    }

    public void OnSliderValueChanged(float value)
    {
        _LGG.gamma.value = new Vector4(value, value, value, value - 1.0f);

        PlayerInfo.GammaSetting = _LGG.gamma.value;
    }

    public void OnDeviceClick(int value)
    {
        switch (value)
        {
            case 1:
                _Controller.SetActive(true);
                _Keyboard.SetActive(false);
                break;
            case 2:
                _Controller.SetActive(false);
                _Keyboard.SetActive(true);
                break;
        }
    }

    public void OnCancelButtonClicked()
    {
        this.gameObject.SetActive(false);
    }

    public void OnExitGameClicked()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void OnMainMenuClicked()
    {
        AkSoundEngine.SetState("GameState", "FrontEnd");
        PhotonNetwork.Disconnect();
    }

    public override void OnEnable()
    {
        base.OnEnable();

        var ES = GameObject.Find("EventSystem");
        ES.GetComponent<EventSystem>().firstSelectedGameObject = GetComponentInChildren<Slider>().gameObject;
        _Controller.SetActive(true);
        _Keyboard.SetActive(false);
    }
}
