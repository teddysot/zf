﻿public static class CustomProperties
{
    // Strings for Photon Custom Properties
    private static string _Bike = "Bike";
    private static string _CheckPoint = "CheckPoint";
    private static string _Lap = "Lap";
    private static string _PowerUp = "PowerUp";
    private static string _PrepareTime = "PrepareTime";
    private static string _Score = "Score";
    private static string _StartTime = "StartTime";
    private static string _EndTime = "EndTime";
    private static string _FinishRace = "FinishRace";
    private static string _TestDrive = "TestDrive";
    private static string _Timespan = "Timespan";
    private static string _WarmUpTime = "WarmUpTime";
    private static string _Map = "Map";
    private static string _HostName = "HostName";

    private static string _Finish1st = "Finish1st";
    private static string _Finish2nd = "Finish2nd";
    private static string _Finish3rd = "Finish3rd";
    private static string _Finish4th = "Finish4th";

    public static string Bike { get { return _Bike; } private set { _Bike = value; } }
    public static string CheckPoint { get { return _CheckPoint; } private set { _CheckPoint = value; } }
    public static string Lap { get { return _Lap; } private set { _Lap = value; } }
    public static string PowerUp { get { return _PowerUp; } private set { _PowerUp = value; } }
    public static string PrepareTime { get { return _PrepareTime; } private set { _PrepareTime = value; } }
    public static string Score { get { return _Score; } private set { _Score = value; } }
    public static string StartTime { get { return _StartTime; } private set { _StartTime = value; } }
    public static string EndTime { get { return _EndTime; } private set { _EndTime = value; } }
    public static string FinishRace { get { return _FinishRace; } private set { _FinishRace = value; } }
    public static string TestDrive { get { return _TestDrive; } private set { _TestDrive = value; } }
    public static string Timespan { get { return _Timespan; } private set { _Timespan = value; } }
    public static string WarmUpTime { get { return _WarmUpTime; } private set { _WarmUpTime = value; } }
    public static string Map { get { return _Map; } private set { _Map = value; } }
    public static string HostName { get { return _HostName; } private set { _HostName = value; } }

    public static string Finish1st { get { return _Finish1st; } private set { _Finish1st = value; } }
    public static string Finish2nd { get { return _Finish2nd; } private set { _Finish2nd = value; } }
    public static string Finish3rd { get { return _Finish3rd; } private set { _Finish3rd = value; } }
    public static string Finish4th { get { return _Finish4th; } private set { _Finish4th = value; } }

    private static string _StatRing = "StatRing";
    private static string _StatNitrous = "StatNitrous";
    private static string _StatHit = "StatHit";

    public static string StatRing { get { return _StatRing; } private set { _StatRing = value; } }
    public static string StatNitrous { get { return _StatNitrous; } private set { _StatNitrous = value; } }
    public static string StatHit { get { return _StatHit; } private set { _StatHit = value; } }
}
