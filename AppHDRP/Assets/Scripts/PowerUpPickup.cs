﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpPickup : MonoBehaviour
{
    private PowerUpManager _PowerUpManager;

    private void Awake() 
    {
        _PowerUpManager = GameObject.Find("PowerUpManager").GetComponent<PowerUpManager>();
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.CompareTag("Player"))
        {
            var player = other.gameObject;
            var playerManager = player.GetComponent<PlayerManager>();
            if(playerManager.CurrentPowerUp != PowerUpType.NONE)
            {
                Destroy(gameObject);
                return;
            }
            _PowerUpManager.GivePowerUp(player);
            Destroy(gameObject);
        }
    }
}