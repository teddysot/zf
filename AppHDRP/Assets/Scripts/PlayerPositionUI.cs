﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PlayerPositionUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _Text;

    private int _Position = -1;

    private int _OwnerId = -1;

    public void SetOwnerId(int id)
    {
        _OwnerId = id;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _Position = PositionManager.Instance.GetCurrentPosition(_OwnerId);
        if (_Position == -1)
        {
            return;
        }

        switch (_Position)
        {
            case 1:
                _Text.text = "1st";
                break;
            case 2:
                _Text.text = "2nd";
                break;
            case 3:
                _Text.text = "3rd";
                break;
            case 4:
                _Text.text = "4th";
                break;
        }
    }
}
