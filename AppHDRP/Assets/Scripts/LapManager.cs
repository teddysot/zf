﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapManager : MonoBehaviour
{
    public void AddLap(ref PlayerManager player)
    {
        player.AddLap(1);
    }
}
