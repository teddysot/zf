﻿using UnityEngine;
using UnityEngine.UI;

public class OffscreenTargetVisualizer : MonoBehaviour
{
    [SerializeField]
    private RectTransform _ArrowRectTransform;

    [SerializeField]
    private Transform _Target;

    [SerializeField, Range(0f, 1f)]
    private float _BorderBuffer = 0.9f;

    private Camera _Cam;

    private void Awake()
    {
        _Cam = Camera.main;
    }


    private void Update()
    {
        Vector3 screenPos = _Cam.WorldToScreenPoint(_Target.position);

        if (screenPos.z > 0f && screenPos.x.IsBetween(0f, Screen.width) && screenPos.y.IsBetween(0f, Screen.height))
        {
            _ArrowRectTransform.gameObject.SetActive(false);
        }
        else // Not Visible
        {
            _ArrowRectTransform.gameObject.SetActive(true);
            if (screenPos.z < 0f) screenPos *= -1; // Flip things when behind us

            Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0f) * 0.5f;
            screenPos -= screenCenter; //Remap coordinates to center of screen instead of BottomLeft

            float angle = Mathf.Atan2(screenPos.y, screenPos.x);
            angle -= 90 * Mathf.Deg2Rad;

            float cos = Mathf.Cos(angle);
            float sin = -Mathf.Sin(angle);
            float hyp = cos / sin;

            Vector3 screenBounds = screenCenter * _BorderBuffer;
            if (cos > 0f) // is Above
            {
                screenPos = new Vector3(screenBounds.y / hyp, screenBounds.y, 0f);
            }
            else // Is Below
            {
                screenPos = new Vector3(-screenBounds.y / hyp, -screenBounds.y, 0f);
            }

            if (screenPos.x > screenBounds.x) // Is Right
            {
                screenPos = new Vector3(screenBounds.x, screenBounds.x * hyp, 0f);
            }
            else if(screenPos.x < -screenBounds.x) // Is Left
            {
                screenPos = new Vector3(-screenBounds.x, -screenBounds.x * hyp, 0f);
            }

            _ArrowRectTransform.localPosition = screenPos;
            _ArrowRectTransform.localRotation = Quaternion.Euler(0f, 0f, angle * Mathf.Rad2Deg);
        }
    }

}

public static class Extensions
{
    public static bool IsBetween(this float val, float min, float max)
    {
        return val > min && val < max;
    }
}