﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviourPunCallbacks
{
    [Header("Screen References")]
    [SerializeField] private GameObject _MainScreen;
    [SerializeField] private GameObject _OptionScreen;
    [SerializeField] private GameObject _CreditScreen;
    [SerializeField] private GameObject _SetNameScreen;
    [SerializeField] private GameObject _SelectBikeScreen;
    [SerializeField] private GameObject _RoomListScreen;
    [SerializeField] private GameObject _LobbyScreen;

    [Header("Bike Selection References")]
    [SerializeField] private GameObject _BikeSelection;
    [SerializeField] private List<Sprite> _BikeSprites;
    [SerializeField] private Image _BikeImage;

    [Header("Audio Options References")]
    [SerializeField] private TextMeshProUGUI _AudioText;
    [SerializeField] private GameObject _AudioOptions;

    [Header("Graphic Options References")]
    [SerializeField] private TextMeshProUGUI _GraphicsText;
    [SerializeField] private GameObject _GraphicOptions;

    [Header("Set Name Screen References")]
    [SerializeField] private TMP_InputField _InputField;
    [SerializeField] private GameObject _AlertText;

    [Header("Room List References")]
    [SerializeField] private GameObject _CreatePanel;

    [SerializeField] private Canvas _Canvas;

    private string _GameplayScene = "SCE_Gameplay";
    private string _NickName = "";

    private GameObject _CurrentScreen;
    private GameObject _PrevScreen;

    private TextMeshProUGUI _CurrentOptionText;
    private GameObject _CurrentOption;

    private int _PlayerNameCount = 0;

    private string _TypeForwardEvent = "Play_TYP_Forward";
    private string _TypeBackwardEvent = "Play_TYP_Backward";

    private void Start()
    {
        _MainScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 2.0f);
    }

    public void OnPlayButtonClicked()
    {
        _MainScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-850.0f, 0.0f), 1.0f);
        _SetNameScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);
        _PrevScreen = _MainScreen;
        _CurrentScreen = _SetNameScreen;
    }

    public void OnOptionButtonClicked()
    {
        _MainScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-850.0f, 0.0f), 1.0f);
        _OptionScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);
        SwitchOption(_AudioOptions, _AudioText);
        _PrevScreen = _MainScreen;
        _CurrentScreen = _OptionScreen;
        _Canvas.renderMode = RenderMode.ScreenSpaceCamera;
    }

    public void OnCreditButtonClicked()
    {
        _MainScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-850.0f, 0.0f), 1.0f);
        _CreditScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);
        _CreditScreen.GetComponent<CreditScreen>().PlayVideo();

        _CurrentScreen = _CreditScreen;
        _PrevScreen = _MainScreen;
        AkSoundEngine.SetState("GameState", "Credits");
    }

    public void OnSetNameClicked()
    {
        if (_InputField.text.Length > 12 || _InputField.text.Length < 3)
        {
            _InputField.text = "";
            _PlayerNameCount = 0;
            AkSoundEngine.PostEvent("Play_UI_Fail", this.gameObject);
            _AlertText.SetActive(true);
            return;
        }

        _NickName = _InputField.text;

        Login();
    }

    public void OnPlayerNameTyping(string input)
    {
        if (input.Length > _PlayerNameCount)
        {
            AkSoundEngine.PostEvent(_TypeForwardEvent, this.gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent(_TypeBackwardEvent, this.gameObject);
        }

        _PlayerNameCount = input.Length;
    }

    public void OnCreateRoomClicked()
    {
        _CreatePanel.SetActive(true);
    }

    private void Login()
    {
        PhotonNetwork.OfflineMode = false;              // true would fake the connection
        PhotonNetwork.AutomaticallySyncScene = true;    // call PhotonNetowork.LoadLevel()
        PhotonNetwork.GameVersion = "V2";               // only people with same version can play together
        PhotonNetwork.LocalPlayer.NickName = _NickName;
        PhotonNetwork.SendRate = 50;
        PhotonNetwork.SerializationRate = 25;
        PhotonNetwork.ConnectUsingSettings();

        PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Bike] = 0;
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        Debug.Log($"[MainMenuManager] Player: {PhotonNetwork.NickName} connected to Master");

        _SetNameScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-850.0f, 0.0f), 1.0f);
        _SelectBikeScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);
        _PrevScreen = _SetNameScreen;
        _CurrentScreen = _SelectBikeScreen;

        PhotonNetwork.JoinLobby();
    }

    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

    public void OnConnectClicked()
    {
        _SelectBikeScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-850.0f, 0.0f), 1.0f);
        _RoomListScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);

        _PrevScreen = _SelectBikeScreen;
        _CurrentScreen = _RoomListScreen;
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Debug.Log($"[MainMenuManager] Master: {PhotonNetwork.IsMasterClient} | Players in Room: {PhotonNetwork.CurrentRoom.PlayerCount} | Room Name: {PhotonNetwork.CurrentRoom.Name}");
        _CurrentScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0.0f, -450.0f), 1.0f).OnComplete(() =>
        {
            SceneManager.LoadScene(_GameplayScene);
        });
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);

        Debug.Log($"[MainMenuManager] Create Room Failed: {message}");
    }

    public void OnSelectBikeClicked()
    {
        _BikeSelection.SetActive(true);
    }

    public void OnBikeClicked(int value)
    {
        _BikeSelection.SetActive(false);
        _BikeImage.sprite = _BikeSprites[value];
        PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Bike] = value;
    }

    public void OnAudiosClicked()
    {
        SwitchOption(_AudioOptions, _AudioText);
    }

    public void OnGraphicsClicked()
    {
        SwitchOption(_GraphicOptions, _GraphicsText);
    }

    public void OnExitClicked()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void OnBackButtonClicked()
    {
        var temp = _CurrentScreen;
        _CurrentScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-850.0f, 0.0f), 1.0f);
        _PrevScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);
        _CurrentScreen = _PrevScreen;
        _PrevScreen = temp;

        if (_CreditScreen == _SetNameScreen)
        {
            _InputField.text = "";
            _PlayerNameCount = 0;
        }
    }

    public void OnBackToMenuClicked()
    {
        if (_CurrentScreen == _CreditScreen)
        {
            AkSoundEngine.SetState("GameState", "FrontEnd");
        }

        _CurrentScreen.GetComponent<RectTransform>().DOAnchorPos(new Vector2(850.0f, 0.0f), 1.0f);
        _MainScreen.GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 1.0f);
        _PrevScreen = _CurrentScreen;
        _CurrentScreen = _MainScreen;

        _InputField.text = "";
        _PlayerNameCount = 0;

        if (PhotonNetwork.IsConnected)
        {
            Disconnect();
        }

        _Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
    }

    public void OnHoverEnter(TextMeshProUGUI text)
    {
        if (_CurrentOptionText == text)
        {
            return;
        }

        text.fontStyle = FontStyles.Underline;
    }

    public void OnCancelClicked()
    {
        _CreatePanel.SetActive(false);
    }

    public void OnHoverExit(TextMeshProUGUI text)
    {
        if (_CurrentOptionText == text)
        {
            return;
        }

        text.fontStyle = FontStyles.Normal;
    }

    public void SwitchOption(GameObject newOption, TextMeshProUGUI newText)
    {
        if (_CurrentOptionText != null)
        {
            _CurrentOptionText.fontStyle = FontStyles.Normal;
        }
        _CurrentOptionText = newText;
        _CurrentOptionText.fontStyle = FontStyles.Underline;

        if (_CurrentOption != null)
        {
            _CurrentOption.SetActive(false);
        }
        _CurrentOption = newOption;
        _CurrentOption.SetActive(true);
    }

    public void SwitchScreen(GameObject newScreen)
    {
        if (_CurrentScreen != null)
        {
            _PrevScreen = _CurrentScreen;
            _CurrentScreen.SetActive(false);
        }
        _CurrentScreen = newScreen;
        _CurrentScreen.SetActive(true);
    }
}
