﻿using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager Instance;

    public int WarmUpStartTime = 0;
    public int PrepareStartTime = 0;
    public int RaceStartTime = 0;
    public int RaceEndTime = 0;
    public string HostName { get; private set; } = "";
    public string MapName { get; private set; } = "";

    [SerializeField] private Leaderboard _Leaderboard;
    [SerializeField] private GameObject _RaceTimerUI;
    [SerializeField] private int _FinishRace;
    [SerializeField] private int[] _FinishTimes = new int[4];

    private ExitGames.Client.Photon.Hashtable _WarmUpValue;
    private ExitGames.Client.Photon.Hashtable _PrepareValue;
    private ExitGames.Client.Photon.Hashtable _RaceValue;
    private ExitGames.Client.Photon.Hashtable _RaceEndValue;
    private ExitGames.Client.Photon.Hashtable _FinishRaceValue;
    private ExitGames.Client.Photon.Hashtable _FinishTime1stValue;
    private ExitGames.Client.Photon.Hashtable _FinishTime2ndValue;
    private ExitGames.Client.Photon.Hashtable _FinishTime3rdValue;
    private ExitGames.Client.Photon.Hashtable _FinishTime4thValue;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    public void WarmUpStart()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            if (_WarmUpValue != null)
            {
                PhotonNetwork.CurrentRoom.SetCustomProperties(_WarmUpValue);
                return;
            }
            _WarmUpValue = new ExitGames.Client.Photon.Hashtable();
            WarmUpStartTime = PhotonNetwork.ServerTimestamp;
            _WarmUpValue.Add(CustomProperties.WarmUpTime, WarmUpStartTime);
            PhotonNetwork.CurrentRoom.SetCustomProperties(_WarmUpValue);
        }

        Debug.Log($"[GameManager] {PhotonNetwork.LocalPlayer.NickName} WarmUpTime : {WarmUpStartTime}");
    }

    public void PrepareStart()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            _PrepareValue = new ExitGames.Client.Photon.Hashtable();
            PrepareStartTime = PhotonNetwork.ServerTimestamp;
            _PrepareValue.Add(CustomProperties.PrepareTime, PrepareStartTime);
            PhotonNetwork.CurrentRoom.SetCustomProperties(_PrepareValue);
        }

        Debug.Log($"[GameManager] {PhotonNetwork.LocalPlayer.NickName} PrepareTime : {PrepareStartTime}");

    }

    public void RaceStart()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            _RaceValue = new ExitGames.Client.Photon.Hashtable();
            RaceStartTime = PhotonNetwork.ServerTimestamp;
            _RaceValue.Add(CustomProperties.StartTime, RaceStartTime);
            PhotonNetwork.CurrentRoom.SetCustomProperties(_RaceValue);
        }

        Debug.Log($"[GameManager] {PhotonNetwork.LocalPlayer.NickName} RaceTime : {RaceStartTime}");
    }

    public void RaceEnd()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            _RaceEndValue = new ExitGames.Client.Photon.Hashtable();
            RaceEndTime = PhotonNetwork.ServerTimestamp;
            _RaceEndValue.Add(CustomProperties.EndTime, RaceEndTime);
            PhotonNetwork.CurrentRoom.SetCustomProperties(_RaceEndValue);
        }

        Debug.Log($"[GameManager] {PhotonNetwork.LocalPlayer.NickName} RaceEndTime : {RaceEndTime}");
    }

    public void FinishRace(int playerId, int timespan)
    {
        if (_FinishTimes[playerId - 1] != 0)
        {
            return;
        }
        
        _FinishTimes[playerId - 1] = timespan;

        switch (playerId)
        {
            case 1:
                _FinishTime1stValue = new ExitGames.Client.Photon.Hashtable();
                _FinishTime1stValue.Add(CustomProperties.Finish1st, timespan);
                PhotonNetwork.CurrentRoom.SetCustomProperties(_FinishTime1stValue);
                break;
            case 2:
                _FinishTime2ndValue = new ExitGames.Client.Photon.Hashtable();
                _FinishTime2ndValue.Add(CustomProperties.Finish2nd, timespan);
                PhotonNetwork.CurrentRoom.SetCustomProperties(_FinishTime2ndValue);
                break;
            case 3:
                _FinishTime3rdValue = new ExitGames.Client.Photon.Hashtable();
                _FinishTime3rdValue.Add(CustomProperties.Finish3rd, timespan);
                PhotonNetwork.CurrentRoom.SetCustomProperties(_FinishTime3rdValue);
                break;
            case 4:
                _FinishTime4thValue = new ExitGames.Client.Photon.Hashtable();
                _FinishTime4thValue.Add(CustomProperties.Finish4th, timespan);
                PhotonNetwork.CurrentRoom.SetCustomProperties(_FinishTime4thValue);
                break;
        }
    }

    public int[] GetFinishTimes()
    {
        return _FinishTimes;
    }

    private void UpdateFinishRace()
    {
        _FinishRaceValue = new ExitGames.Client.Photon.Hashtable();
        _FinishRace++;
        _FinishRaceValue.Add(CustomProperties.FinishRace, _FinishRace);
        PhotonNetwork.CurrentRoom.SetCustomProperties(_FinishRaceValue);
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        Debug.Log($"[GameManager] Room Properties Update");

        object Finish1st;
        object Finish2nd;
        object Finish3rd;
        object Finish4th;

        if (propertiesThatChanged.TryGetValue(CustomProperties.Finish1st, out Finish1st))
        {
            _FinishTimes[0] = (int)Finish1st;
            UpdateFinishRace();

            Debug.Log($"[GameManager] Finish1st : {Finish1st}");
        }

        if (propertiesThatChanged.TryGetValue(CustomProperties.Finish2nd, out Finish2nd))
        {
            _FinishTimes[1] = (int)Finish2nd;
            UpdateFinishRace();

            Debug.Log($"[GameManager] Finish2nd : {Finish2nd}");
        }

        if (propertiesThatChanged.TryGetValue(CustomProperties.Finish3rd, out Finish3rd))
        {
            _FinishTimes[2] = (int)Finish3rd;
            UpdateFinishRace();

            Debug.Log($"[GameManager] Finish3rd : {Finish3rd}");
        }

        if (propertiesThatChanged.TryGetValue(CustomProperties.Finish4th, out Finish4th))
        {
            _FinishTimes[3] = (int)Finish4th;
            UpdateFinishRace();

            Debug.Log($"[GameManager] Finish4rd : {Finish4th}");
        }

        object finish;

        if (propertiesThatChanged.TryGetValue(CustomProperties.FinishRace, out finish))
        {
            _FinishRace = (int)finish;

            if (_FinishRace == PhotonNetwork.CurrentRoom.PlayerCount)
            {
                _Leaderboard.ShowBoard();
                _RaceTimerUI.SetActive(false);
                Debug.Log($"[GameManager] Player Count: {PhotonNetwork.CurrentRoom.PlayerCount}");
            }

            Debug.Log($"[GameManager] FinishRace : {finish}");
        }
    }
}
