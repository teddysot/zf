﻿public enum CombatType
{
    REGULAR,
    CHARGED,
    KNOCKOFF,
    BLOCK,
}

public enum PowerUpType
{
    NONE,
    STUNMINE,
    IONCANNON,
    ZEROPULSE,
    ZEROBOLT
}

public enum RacingType
{
    RING,
    MOSTRINGS,
    LAPFIRST
}

public enum LapAchievementType
{
    MOSTNORMALHITS,
    MOSTCHARGEDHITS,
    MOSTBLOCKS,
    MOSTKNOCKOFFS
}

public enum RaceAchievementType
{
    MOSTNORMALHITS,
    MOSTCHARGEDHITS,
    MOSTBLOCKS,
    MOSTKNOCKOFFS
}

public enum RaceOutcomeType
{
    FIRST,
    SECOND,
    THIRD,
    FOURTH
}