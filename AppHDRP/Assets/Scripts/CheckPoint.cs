﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] private int _NoCheckPoint = 0;
    [SerializeField] private Transform _RefTransform;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var player = other.GetComponent<PlayerManager>();

            if (_RefTransform == null)
            {
                player.SetCheckpoint(this.transform.position, this.transform.rotation, _NoCheckPoint);
            }
            else
            {
                player.SetCheckpoint(_RefTransform.position, _RefTransform.rotation, _NoCheckPoint);
            }
        }
    }
}
