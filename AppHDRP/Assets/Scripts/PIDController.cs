﻿using UnityEngine;

[System.Serializable]
public class PIDController
{
    // PID Coefficients for tuning the controller
    [SerializeField] private float _PCoeff = 0.8f;
    [SerializeField] private float _ICoeff = 0.0002f;
    [SerializeField] private float _DCoeff = 0.2f;
    [SerializeField] private float _Minimum = -1;
    [SerializeField] private float _Maximum = 1.0f;

    // Variables to store values between calculations
    private float _Integral;
    private float _LastProportional;

    // Passing in the desired value and the cuurent value
    // Function return a number that moves vehicle towards
    public float Seek(float seekValue, float currentValue)
    {
        float deltaTime = Time.fixedDeltaTime;
        float proportional = seekValue - currentValue;

        float derivative = (proportional - _LastProportional) / deltaTime;
        _Integral += proportional * deltaTime;
        _LastProportional = proportional;

        // This is the actual PID formula
        float value = (_PCoeff * proportional) + (_ICoeff * _Integral) + (_DCoeff * derivative);
        value = Mathf.Clamp(value, _Minimum, _Maximum);

        return value;
    }
}
