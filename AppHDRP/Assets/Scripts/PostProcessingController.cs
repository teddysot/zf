﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.HDPipeline;
using DG.Tweening;
using Cinemachine;
using Photon.Pun;

public class PostProcessingController : MonoBehaviourPunCallbacks
{
    [SerializeField] private CinemachineVirtualCamera _VCam;
    private GameObject _PostProcessingGO;
    private Volume _Volume;

    private Vignette _Vignette;
    private MotionBlur _MotionBlur;
    private ChromaticAberration _ChromaticAberration;

    private float _VignetteValue = 0.0f;
    private float _ChromaticAberrationValue = 0.0f;

    private void Awake()
    {
        _PostProcessingGO = GameObject.FindGameObjectWithTag("PostProcessing");
        _Volume = _PostProcessingGO.GetComponent<Volume>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Vignette tmpVgn;
        if (_Volume.profile.TryGet<Vignette>(out tmpVgn))
        {
            _Vignette = tmpVgn;
            _Vignette.intensity.Override(_VignetteValue);
        }

        MotionBlur tmpMB;
        if (_Volume.profile.TryGet<MotionBlur>(out tmpMB))
        {
            _MotionBlur = tmpMB;
        }

        ChromaticAberration tmpCA;
        if (_Volume.profile.TryGet<ChromaticAberration>(out tmpCA))
        {
            _ChromaticAberration = tmpCA;
        }
    }

    public void EnableBoostEffect()
    {
        DOTween.To(() => _VignetteValue, x => _VignetteValue = x, 0.2f, 1.0f).OnPlay(() =>
        _Vignette.intensity.Override(_VignetteValue)
        );

        DOTween.To(() => _VCam.m_Lens.FieldOfView, x => _VCam.m_Lens.FieldOfView = x, 70.0f, 1.0f);

        DOTween.To(() => _ChromaticAberrationValue, x => _ChromaticAberrationValue = x, 1.0f, 1.0f).OnPlay(() =>
        _ChromaticAberration.intensity.Override(_ChromaticAberrationValue)
        );
    }

    public void DisableBoostEffect()
    {
        DOTween.To(() => _VignetteValue, x => _VignetteValue = x, 0.0f, 2.0f).OnPlay(() =>
        _Vignette.intensity.Override(_VignetteValue)
        );

        DOTween.To(() => _VCam.m_Lens.FieldOfView, x => _VCam.m_Lens.FieldOfView = x, 60.0f, 2.0f);

        DOTween.To(() => _ChromaticAberrationValue, x => _ChromaticAberrationValue = x, 0.0f, 2.0f).OnPlay(() =>
        _ChromaticAberration.intensity.Override(_ChromaticAberrationValue)
        );
    }
}
