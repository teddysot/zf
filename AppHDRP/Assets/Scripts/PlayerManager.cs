﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Experimental.Rendering.HDPipeline;
using UnityEngine.Rendering;
using System;
using UnityEngine.Playables;
using Cinemachine;

public class PlayerManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private int _Score = 0;
    public int Score { get { return _Score; } private set { _Score = value; } }

    [SerializeField] private int _Bike = 0;
    [SerializeField] private int _Lap = 0;
    [SerializeField] private int _Checkpoint = 0;
    public int CheckPoint { get { return _Checkpoint; } private set { _Checkpoint = value; } }
    [SerializeField] private int _CheckpointCount = 0;
    public int CheckpointCount { get { return _CheckpointCount; } private set { _CheckpointCount = value; } }

    [SerializeField] private PowerUpType _CurrentPowerUp;
    public PowerUpType CurrentPowerUp { get { return _CurrentPowerUp; } private set { _CurrentPowerUp = value; } }
    [SerializeField] private Vector3 _LastCheckpoint;
    [SerializeField] private Quaternion _LastCheckrot;

    [SerializeField] private Vector4 _GammaSettings = new Vector4(1.00f, 1.00f, 1.00f, 0.00f);
    [SerializeField] private PlayableDirector _Timeline;
    [SerializeField] private CinemachineVirtualCamera _VCam;

    private GameObject _PostProcessingGO;
    private Volume _Volume;
    private LiftGammaGain _LGG;

    private HUDManager _HUDManager;
    private PlayerNameUI _PlayerNameUI;
    private PlayerScoreUI _PlayerScoreUI;
    private PlayerPowerUpUI _PlayerPowerUpUI;
    private PlayerNitrousBar _PlayerNitrousBar;
    private PlayerPositionUI _PlayerPositionUI;
    private PlayerLapUI _PlayerLapUI;
    private PlayerSpeedMeterUI _PlayerSpeedMeterUI;
    private PlayerOptionsUI _PlayerOptionsUI;
    public PlayerOptionsUI PlayerOptionsUI { get { return _PlayerOptionsUI; } private set { _PlayerOptionsUI = value; } }

    private Rigidbody _Rigidbody;
    private VehicleMovement _VehicleMovement;
    private PlayerInput _PlayerInput;

    private float _RespawnHolding;

    private GameplaySFX _GameplaySFX;
    private RespawnEffect _RespawnEffect;

    private void Awake()
    {
        _VehicleMovement = GetComponent<VehicleMovement>();
        _PlayerInput = GetComponent<PlayerInput>();
        _HUDManager = GetComponentInChildren<HUDManager>();
        _Rigidbody = GetComponent<Rigidbody>();
        _GameplaySFX = GetComponent<GameplaySFX>();
        _PostProcessingGO = GameObject.FindGameObjectWithTag("PostProcessing");
        _Volume = _PostProcessingGO.GetComponent<Volume>();
        _RespawnEffect = GetComponent<RespawnEffect>();
        photonView.Owner.CustomProperties[CustomProperties.Score] = 0;
        _Lap = 1;
        photonView.Owner.CustomProperties[CustomProperties.Lap] = 1;
        _CurrentPowerUp = PowerUpType.NONE;
        photonView.Owner.CustomProperties[CustomProperties.PowerUp] = _CurrentPowerUp;
        photonView.Owner.CustomProperties[CustomProperties.Timespan] = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        LiftGammaGain tmp;
        if (_Volume.profile.TryGet<LiftGammaGain>(out tmp))
        {
            _LGG = tmp;
            _LGG.gamma.value = PlayerInfo.GammaSetting;
        }

        CreateHUD();

        photonView.Owner.TagObject = this.gameObject;
        PositionManager.Instance.InitilizePlayerList();
        Debug.Log($"[PlayerManager] {photonView.Owner.NickName} set tag object to {this.gameObject.name}");
    }

    private void Update()
    {
        if (LoadingScreen.Instance.IsComplete)
        {
            _PlayerNameUI.gameObject.SetActive(true);
            _PlayerSpeedMeterUI.UpdateSpeed(_VehicleMovement.Speed);

            if (_PlayerInput.IsRespawn)
            {
                CheckRespawnHolding();
            }
            else
            {
                _RespawnHolding = 0.0f;
            }
        }
    }
    private void CheckRespawnHolding()
    {
        _RespawnHolding += Time.deltaTime;

        if (_RespawnHolding >= 3.0f)
        {
            _RespawnHolding = 0.0f;
            Respawn();
        }
    }

    private void CreateHUD()
    {
        if (_HUDManager != null)
        {
            CreateNameUI();
            _PlayerOptionsUI = _HUDManager.PlayerOptionsUI;
            _PlayerPowerUpUI = _HUDManager.PlayerPowerUpUI;
            _PlayerNitrousBar = _HUDManager.PlayerNitrousBar;
            _PlayerPositionUI = _HUDManager.PlayerPositionUI;
            _PlayerLapUI = _HUDManager.PlayerLapUI;
            _PlayerSpeedMeterUI = _HUDManager.PlayerSpeedMeterUI;

            _PlayerPositionUI.SetOwnerId(photonView.Owner.ActorNumber);
            _PlayerLapUI.UpdateLap(_Lap);

            //_PlayerScoreUI.UpdateScore();
        }
        else
        {
            Debug.LogError("[PlayerManager] Missing HUDManager reference");
        }
    }

    private void CreateNameUI()
    {
        _PlayerNameUI = _HUDManager.CreateNameUI();
        _PlayerNameUI.gameObject.SetActive(false);

        if (_PlayerNameUI != null)
        {
            _PlayerNameUI.SetTarget(this.gameObject);
            var HUD = GameObject.FindGameObjectWithTag("HUD");
            _PlayerNameUI.transform.SetParent(HUD.GetComponentInChildren<HUDManager>().transform);
        }
    }

    public void UpdateNitrousBar(float value)
    {
        _PlayerNitrousBar.UpdateBar(value);
    }

    public void AddScore(int value)
    {
        var playerScoreUI = _PlayerScoreUI.GetComponent<PlayerScoreUI>();

        _Score += value;

        PhotonNetwork.LocalPlayer.CustomProperties[CustomProperties.Score] = _Score;

        //playerScoreUI.UpdateScore();

        //Debug.Log($"[PlayerManager] {PhotonNetwork.LocalPlayer.NickName} got {value} points!");
    }

    public void AddLap(int value)
    {
        _Lap += value;

        _CheckpointCount = 0;

        var position = PositionManager.Instance.GetCurrentPosition(photonView.Owner.ActorNumber);

        if (_Lap > 3)
        {
            _HUDManager.gameObject.SetActive(false);
            _PlayerInput.FinishRace();
            var timespan = PhotonNetwork.ServerTimestamp - GameManager.Instance.RaceStartTime;
            photonView.Owner.CustomProperties[CustomProperties.Timespan] = timespan;

            if (photonView.IsMine)
            {
                switch (position)
                {
                    case 1:
                        _GameplaySFX.PlayStinger(1);
                        break;
                    case 2:
                        _GameplaySFX.PlayStinger(2);
                        break;
                    case 3:
                        _GameplaySFX.PlayStinger(4);
                        break;
                    case 4:
                        _GameplaySFX.PlayStinger(4);
                        break;
                }

            }

            _Rigidbody.velocity *= 0.0f;
            _Rigidbody.angularVelocity *= 0.0f;

            _Timeline.Play();

            _VCam.LookAt = _Timeline.gameObject.transform;
            _VCam.Follow = _Timeline.gameObject.transform;

            GameManager.Instance.FinishRace(photonView.Owner.ActorNumber, timespan);
            Debug.Log($"[PlayerManager] Finish Race");
            return;
        }

        _Checkpoint = 0;
        photonView.Owner.CustomProperties[CustomProperties.CheckPoint] = 0;
        PositionManager.Instance.UpdateCheckpoint(0, photonView.Owner.ActorNumber);

        if (photonView.IsMine)
        {
            _GameplaySFX.PlayLapSFX(_Lap);

            switch (_Lap)
            {
                case 2:
                    AkSoundEngine.SetSwitch("Lap", "L2", this.gameObject);
                    break;
                case 3:
                    AkSoundEngine.SetSwitch("Lap", "L4", this.gameObject);
                    AkSoundEngine.PostEvent("Play_VO_FinalLap", this.gameObject);
                    break;
            }
        }

        photonView.Owner.CustomProperties[CustomProperties.Lap] = _Lap;

        PositionManager.Instance.UpdateLap(_Lap, photonView.Owner.ActorNumber);
        _PlayerLapUI.UpdateLap(_Lap);

        Debug.Log($"[PlayerManager] {photonView.Owner.NickName} is on {_Lap} lap");
    }

    public void CollectPowerUp(PowerUpType type)
    {
        if (_CurrentPowerUp != PowerUpType.NONE)
        {
            return;
        }

        photonView.Owner.CustomProperties[CustomProperties.PowerUp] = type;
        _CurrentPowerUp = (PowerUpType)photonView.Owner.CustomProperties[CustomProperties.PowerUp];
        //Debug.Log($"[PlayerManager] {PhotonNetwork.LocalPlayer.NickName} collected {type}");
    }

    public void ShufflePowerUp(int index)
    {
        _PlayerPowerUpUI.UpdateImage(index);
    }

    public void PlayPowerUpSFX()
    {
        _GameplaySFX.PlayPowerUpShuffle();
    }

    public void PlayLastPowerUpSFX()
    {
        _GameplaySFX.PlayLastPowerUpShuffle();
    }

    public void Respawn()
    {
        _Rigidbody.velocity *= 0;
        transform.position = _LastCheckpoint;
        transform.rotation = _LastCheckrot;
        _GameplaySFX.PlayRespawn();
        _RespawnEffect.Activate();
    }

    public void ActivateZeroPulse()
    {
        _PlayerInput.ActivateZeroPulse();
    }

    public void SetCheckpoint(Vector3 pos, Quaternion rot, int NumCheckPoint)
    {
        if (_Checkpoint >= NumCheckPoint)
        {
            return;
        }

        _LastCheckpoint = pos;
        _LastCheckrot = rot;

        _Checkpoint = NumCheckPoint;
        _CheckpointCount++;

        photonView.Owner.CustomProperties[CustomProperties.CheckPoint] = NumCheckPoint;
        PositionManager.Instance.UpdateCheckpoint(NumCheckPoint, photonView.Owner.ActorNumber);

        if (_Lap == 4)
        {
            var position = PositionManager.Instance.GetCurrentPosition(photonView.Owner.ActorNumber);
            switch (position)
            {
                case 1:
                    Debug.Log($"Set Position: {position}");
                    AkSoundEngine.SetSwitch("Position", "P1", this.gameObject);
                    AkSoundEngine.SetState("Position", "One");
                    break;
                case 2:
                    Debug.Log($"Set Position: {position}");
                    AkSoundEngine.SetSwitch("Position", "P2", this.gameObject);
                    AkSoundEngine.SetState("Position", "Two");
                    break;
                case 3:
                    Debug.Log($"Set Position: {position}");
                    AkSoundEngine.SetSwitch("Position", "P3", this.gameObject);
                    AkSoundEngine.SetState("Position", "Three");
                    break;
                case 4:
                    Debug.Log($"Set Position: {position}");
                    AkSoundEngine.SetSwitch("Position", "P4", this.gameObject);
                    AkSoundEngine.SetState("Position", "Four");
                    break;
            }
        }
    }

    public void ClearPowerUp()
    {
        _CurrentPowerUp = PowerUpType.NONE;
        _PlayerPowerUpUI.UpdateImage(PowerUpType.NONE);
    }

    public void ClearScore()
    {
        _Score = 0;
        photonView.Owner.CustomProperties[CustomProperties.Score] = _Score;
        _PlayerScoreUI.GetComponent<PlayerScoreUI>().UpdateScore();
    }
}
