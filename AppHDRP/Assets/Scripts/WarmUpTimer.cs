﻿using UnityEngine;
using Photon.Pun;
using System;

public class WarmUpTimer : MonoBehaviourPunCallbacks
{
    private RaceTimerUI _RaceTimerUI;

    private PrepareTimer _PrepareTimer;

    public delegate void WarmUpTimerHasExpired();

    public static event WarmUpTimerHasExpired OnWarmUpTimerHasExpired;

    private bool _IsTimerRunning;

    private int _StartTime;

    [Header("Countdown time in seconds")]
    public int Countdown = 30;

    private void Awake()
    {
        _RaceTimerUI = GetComponent<RaceTimerUI>();
        _PrepareTimer = GetComponent<PrepareTimer>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) || (Input.GetKeyDown(KeyCode.JoystickButton6)) || LoadingScreen.Instance.IsComplete && PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
        {
            WarmUpStart();
        }

        if (_IsTimerRunning == false)
        {
            return;
        }

        if (_StartTime == 0)
        {
            return;
        }

        var timer = PhotonNetwork.ServerTimestamp - _StartTime;
        var countdown = Countdown - TimeSpan.FromMilliseconds(timer).Seconds;

        _RaceTimerUI.UpdateWarmUpTime(countdown);

        if (countdown > 0)
        {
            return;
        }

        _IsTimerRunning = false;

        if (OnWarmUpTimerHasExpired != null)
        {
            OnWarmUpTimerHasExpired();
        }

        _RaceTimerUI.ClearTime();
        PositionManager.Instance.InitilizePlayerList();
        _PrepareTimer.PrepareStart();
        this.enabled = false;
    }

    public void WarmUpStart()
    {
        GameManager.Instance.WarmUpStart();
        _StartTime = GameManager.Instance.WarmUpStartTime;
        _IsTimerRunning = true;
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient == false)
        {
            object warmUpTime;

            if (propertiesThatChanged.TryGetValue(CustomProperties.WarmUpTime, out warmUpTime))
            {
                _StartTime = (int)warmUpTime;
                GameManager.Instance.WarmUpStartTime = _StartTime;
                _IsTimerRunning = true;

                Debug.Log($"[WarmUpTimer] {PhotonNetwork.LocalPlayer.NickName} WarmUp2 : {warmUpTime}");
            }
        }
    }
}