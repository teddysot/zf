﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Reflection : MonoBehaviourPunCallbacks
{
    [SerializeField] private float _Force = 3.0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sword"))
        {
            Debug.Log("Hit");
            // Calculate angle between the collision point and the player
            Vector3 direction = other.transform.position - transform.position;

            // Get the opposite Vector3 and normalize it
            direction = -direction.normalized;

            direction = new Vector3(direction.x, 0.0f, direction.z);

            // Add force in the direction of the contact point and multiply it by force
            GetComponent<Rigidbody>().AddForce(direction * _Force, ForceMode.Impulse);
        }

        if(this.CompareTag("Props") && other.CompareTag("Player"))
        {
                        // Calculate angle between the collision point and the player
            Vector3 direction = other.transform.position - transform.position;

            // Get the opposite Vector3 and normalize it
            direction = -direction.normalized;

            // Add force in the direction of the contact point and multiply it by force
            GetComponent<Rigidbody>().AddForce(direction * _Force, ForceMode.Impulse);

        }
    }
}