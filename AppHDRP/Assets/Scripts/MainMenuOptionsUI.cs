﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.HDPipeline;

public class MainMenuOptionsUI : MonoBehaviourPunCallbacks
{
    private GameObject _PostProcessingGO;
    private Volume _Volume;
    private LiftGammaGain _LGG;

    private void Awake()
    {
        _PostProcessingGO = GameObject.FindGameObjectWithTag("PostProcessing");
        _Volume = _PostProcessingGO.GetComponent<Volume>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.LogError("[MainMenuOptionsUI] Missing LiftGammaGain in Volume");
    }

    public void OnSliderValueChanged(float value)
    {
        _LGG.gamma.value = new Vector4(value, value, value, value - 1.0f);

        PlayerInfo.GammaSetting = _LGG.gamma.value;
    }
}
