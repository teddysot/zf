﻿using UnityEngine;
using Photon.Pun;

public class PlayerPowerUpController : MonoBehaviourPunCallbacks
{
    [Header("PowerUp Prefabs")]
    [SerializeField] private GameObject _StunMinePrefab;
    [SerializeField] private GameObject _IonCannonPrefab;
    [SerializeField] private GameObject _ZeroPulsePrefab;
    [SerializeField] private GameObject _ZeroBoltPrefab;

    [Header("PowerUp Spawn Positions")]
    [SerializeField] private Transform _StunMinePosition;
    [SerializeField] private Transform _IonCannonPosition;
    [SerializeField] private Transform _ZeroPulsePosition;
    [SerializeField] private Transform _ZeroBoltPosition;

    [Header("Hit VFX")]
    [SerializeField] private GameObject _ZeroPulseHitVFX;
    [SerializeField] private GameObject _IonCannonHitVFX;
    [SerializeField] private GameObject _StunMineHitVFX;
    [SerializeField] private GameObject _ZeroBoltHitVFX;

    private AnimationManager _AnimationManager;
    private PlayerManager _PlayerManager;
    private PlayerInput _PlayerInput;
    private Rigidbody _Rigidbody;
    private VehicleMovement _VehicleMovement;
    private GameplaySFX _GameplaySFX;

    // Start is called before the first frame update
    void Start()
    {
        _AnimationManager = GetComponent<AnimationManager>();
        _PlayerManager = GetComponent<PlayerManager>();
        _PlayerInput = GetComponent<PlayerInput>();
        _Rigidbody = GetComponent<Rigidbody>();
        _VehicleMovement = GetComponent<VehicleMovement>();
        _GameplaySFX = GetComponent<GameplaySFX>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_PlayerInput.IsPowerUp)
        {
            switch (_PlayerManager.CurrentPowerUp)
            {
                case PowerUpType.NONE:
                    break;
                case PowerUpType.STUNMINE:
                    StunMine();
                    break;
                case PowerUpType.IONCANNON:
                    IonCannon();
                    _GameplaySFX.PlayPUP(5);
                    break;
                case PowerUpType.ZEROPULSE:
                    ZeroPulse();
                    _GameplaySFX.PlayPUP(4);
                    break;
                case PowerUpType.ZEROBOLT:
                    ZeroBolt();
                    break;
            }

            //Debug.Log($"[PlayerPowerUpController] {PhotonNetwork.LocalPlayer.NickName} activated {_PlayerManager.CurrentPowerUp}");
            _PlayerManager.ClearPowerUp();
        }
    }

    private void StunMine()
    {
        //Instantiate(_StunMinePrefab, _StunMinePosition.position, Quaternion.identity);
        //PhotonNetwork.InstantiateSceneObject(_StunMinePrefab.name, _StunMinePosition.position, Quaternion.identity);
        PhotonNetwork.Instantiate(_StunMinePrefab.name, _StunMinePosition.position, Quaternion.identity);

        Debug.Log($"{photonView.Owner.NickName} use StunMine");
    }

    private void IonCannon()
    {
        //var ionCannon = Instantiate(_IonCannonPrefab, _ZeroPulsePosition.position, Quaternion.LookRotation(transform.forward));
        //var ionCannon = PhotonNetwork.InstantiateSceneObject(_IonCannonPrefab.name, _IonCannonPosition.position, Quaternion.LookRotation(transform.forward));
        var ionCannon = PhotonNetwork.Instantiate(_IonCannonPrefab.name, _IonCannonPosition.position, Quaternion.LookRotation(transform.forward));

        ionCannon.transform.SetParent(_IonCannonPosition);
        ionCannon.transform.localPosition = Vector3.zero;
        Debug.Log($"{photonView.Owner.NickName} use IonCannon");
    }

    private void ZeroPulse()
    {
        //var zeroPulse = Instantiate(_ZeroPulsePrefab, _ZeroPulsePosition.position, Quaternion.identity);
        //var zeroPulse = PhotonNetwork.InstantiateSceneObject(_ZeroPulsePrefab.name, _ZeroPulsePosition.position, Quaternion.identity);
        var zeroPulse = PhotonNetwork.Instantiate(_ZeroPulsePrefab.name, _ZeroPulsePosition.position, Quaternion.identity);
        zeroPulse.transform.SetParent(_ZeroPulsePosition);
        zeroPulse.transform.localPosition = Vector3.zero;
        Debug.Log($"{photonView.Owner.NickName} use ZeroPulse");
    }

    private void ZeroBolt()
    {
        foreach (var player in PositionManager.Instance.GetPlayers())
        {
            if (player.GetPhotonView().OwnerActorNr == photonView.OwnerActorNr)
            {
                continue;
            }

            player.GetComponent<VehicleMovement>().ActivateZeroBolt();
            Debug.Log($"[{this.ToString()} {photonView.Owner.NickName} uses ZeroVortex to {player.GetComponent<PhotonView>().Owner.NickName}]");
        }
    }

    private void ActivateBolt()
    {
        var zeroBolt = PhotonNetwork.Instantiate(_ZeroBoltPrefab.name, _ZeroBoltPosition.position, Quaternion.identity);
        zeroBolt.transform.SetParent(_ZeroBoltPosition);
        zeroBolt.transform.localPosition = Vector3.zero;
    }

    public void StunMineHit()
    {
        var vfx = PhotonNetwork.Instantiate(_StunMineHitVFX.name, transform.position, transform.rotation);
        vfx.transform.SetParent(transform);
        Destroy(vfx, 2.0f);

        _Rigidbody.velocity *= 0.40f;
        //_AnimationManager.UpdateStunAnimation();
        Debug.Log($"{photonView.Owner.NickName} got hit by StunMine");
    }

    public void IonCannonHit()
    {
        var vfx = PhotonNetwork.Instantiate(_IonCannonHitVFX.name, transform.position, transform.rotation);
        vfx.transform.SetParent(transform);
        Destroy(vfx, 2.0f);

        _Rigidbody.velocity *= 0.00f;
        Debug.Log($"{photonView.Owner.NickName} got hit by IonCannon");
    }

    public void ZeroPulseHit()
    {
        var vfx = PhotonNetwork.Instantiate(_ZeroPulseHitVFX.name, transform.position, transform.rotation);
        vfx.transform.SetParent(transform);
        Destroy(vfx, 2.0f);

        _PlayerManager.ActivateZeroPulse();
        _Rigidbody.velocity *= 0.60f;
        Debug.Log($"{photonView.Owner.NickName} got hit by ZeroPulse");
    }

    public void ZeroBoltHit()
    {
        var vfx = PhotonNetwork.Instantiate(_ZeroBoltHitVFX.name, transform.position, transform.rotation);
        vfx.transform.SetParent(transform);
        Destroy(vfx, 2.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ZeroPulse"))
        {
            ZeroPulseHit();
        }

        if (other.CompareTag("StunMine"))
        {
            StunMineHit();
            _GameplaySFX.PlayStunMineHit();
        }

        if (other.CompareTag("IonCannon"))
        {
            IonCannonHit();
        }
    }
}
