﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour
{
    [SerializeField] private int _RingNum = 0;
    [SerializeField] private GameObject _RingNormal;
    [SerializeField] private GameObject _RingPassed;

    [SerializeField] private float _FillNitrousAmount = 5.0f;

    private bool _IsPassed = false;
    private float _Cooldown = 3.0f;

    private void Start()
    {
        _RingNormal.SetActive(true);
        _RingPassed.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (_IsPassed == false)
        {
            return;
        }

        _Cooldown -= Time.fixedDeltaTime;
        if (_Cooldown <= 0.0f)
        {
            _RingNormal.SetActive(true);
            _RingPassed.SetActive(false);
            _IsPassed = false;
            _Cooldown = 3.0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _RingNormal.SetActive(false);
            _RingPassed.SetActive(true);
            _IsPassed = true;
            _Cooldown = 3.0f;

            var sfx = other.GetComponent<SFXPlayer>();
            sfx.PlayRingSFX(_RingNum);
        }
    }
}
