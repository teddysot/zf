﻿using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(DebugCamera))]
[RequireComponent(typeof(DebugPath))]
[RequireComponent(typeof(DebugVehicle))]
public class DebugMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private bool _DebugMenuEnable = false;
    [SerializeField] private bool _DebugCameraEnable = false;
    [SerializeField] private bool _DebugPathEnable = false;

    public bool DebugMenuEnable => _DebugMenuEnable;
    public bool DebugCameraEnable => _DebugCameraEnable;
    public bool DebugPathEnable => _DebugPathEnable;

    private DebugCamera _DebugCamera;
    private DebugPath _DebugPath;
    private DebugVehicle _DebugVehicle;

    // Start is called before the first frame update
    void Awake()
    {
        _DebugCamera = GetComponent<DebugCamera>();
        _DebugPath = GetComponent<DebugPath>();
        _DebugVehicle = GetComponent<DebugVehicle>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            _DebugMenuEnable = !_DebugMenuEnable;

            _DebugCameraEnable = false;
            _DebugPathEnable = false;

            _DebugCamera.SetEnable(_DebugCameraEnable);
            _DebugPath.SetEnable(_DebugPathEnable);
        }

        if (_DebugMenuEnable)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && _DebugCameraEnable)
            {
                _DebugCamera.RespawnAtCamera();
                _DebugCameraEnable = false;
                _DebugCamera.SetEnable(_DebugCameraEnable);
            }

            if (Input.GetKeyDown(KeyCode.F2))
            {
                _DebugCameraEnable = !_DebugCameraEnable;
                _DebugCamera.SetEnable(_DebugCameraEnable);
            }

            if (Input.GetKeyDown(KeyCode.F3))
            {
                _DebugPathEnable = !_DebugPathEnable;
                _DebugPath.SetEnable(_DebugPathEnable);
            }

            if (Input.GetKeyDown(KeyCode.F4))
            {
                _DebugVehicle.SetDriveForce(1);
            }

            if (Input.GetKeyDown(KeyCode.F5))
            {
                _DebugVehicle.SetDriveForce(-1);
            }

            if (Input.GetKeyDown(KeyCode.F6))
            {
                _DebugVehicle.SetHoverHeight(0.1f);
            }

            if (Input.GetKeyDown(KeyCode.F7))
            {
                _DebugVehicle.SetHoverHeight(-0.1f);
            }

            if (Input.GetKeyDown(KeyCode.F8))
            {
                _DebugVehicle.SetAngleOfRoll(1);
            }

            if (Input.GetKeyDown(KeyCode.F9))
            {
                _DebugVehicle.SetAngleOfRoll(-1);
            }

            if (Input.GetKeyDown(KeyCode.F11))
            {
                _DebugVehicle.ResetSettings();
            }

            if (Input.GetKeyDown(KeyCode.F12))
            {
                _DebugVehicle.ResetGame();
            }

            if (Input.GetKeyDown(KeyCode.Insert))
            {
                _DebugVehicle.SetMaxSpeed(1);
            }

            if (Input.GetKeyDown(KeyCode.Delete))
            {
                _DebugVehicle.SetMaxSpeed(-1);
            }

            if (Input.GetKeyDown(KeyCode.Home))
            {
                _DebugVehicle.SetMaxHoverHeight(0.1f);
            }

            if (Input.GetKeyDown(KeyCode.End))
            {
                _DebugVehicle.SetMaxHoverHeight(-0.1f);
            }
        }
    }

    private void OnGUI()
    {
        GUIStyle green = new GUIStyle();
        GUIStyle red = new GUIStyle();
        GUIStyle yellow = new GUIStyle();
        GUIStyle meter = new GUIStyle();

        green.normal.textColor = Color.green;
        green.alignment = TextAnchor.MiddleLeft;

        red.normal.textColor = Color.red;
        red.alignment = TextAnchor.MiddleLeft;

        yellow.normal.textColor = Color.yellow;
        yellow.alignment = TextAnchor.MiddleLeft;

        GUI.Label(new Rect(Screen.width - 200, 10, 100, 20), "[F1] Debug Menu: ", green);

        if (_DebugMenuEnable)
        {
            GUI.Label(new Rect(Screen.width - 70, 11, 100, 20), "ON", yellow);

            GUI.Label(new Rect(Screen.width - 200, 30, 100, 20), "[F2] Debug Camera: ", green);

            GUI.Label(new Rect(Screen.width - 200, 50, 100, 20), "[F3] Debug Path: ", green);

            GUI.Label(new Rect(Screen.width - 200, 70, 100, 20), "[TAB] Start Game: ", green);

            GUI.Label(new Rect(Screen.width - 500, 10, 100, 20), "[F4/F5] Drive Force: ", green);
            GUI.Label(new Rect(Screen.width - 310, 11, 100, 20), $"{(int)_DebugVehicle.GetDriveForce()}", yellow);

            GUI.Label(new Rect(Screen.width - 500, 30, 100, 20), "[F6/F7] Hover Height: ", green);
            GUI.Label(new Rect(Screen.width - 310, 31, 100, 20), $"{_DebugVehicle.GetHoverHeight().ToString("0.0")}", yellow);

            GUI.Label(new Rect(Screen.width - 500, 50, 100, 20), "[F8/F9] Angle of Roll: ", green);
            GUI.Label(new Rect(Screen.width - 310, 51, 100, 20), $"{(int)_DebugVehicle.GetAngleOfRoll()}", yellow);

            GUI.Label(new Rect(Screen.width - 500, 70, 100, 20), "[INSERT/DELETE] Max Speed: ", green);
            GUI.Label(new Rect(Screen.width - 310, 71, 100, 20), $"{(int)_DebugVehicle.GetMaxSpeed()}", yellow);

            GUI.Label(new Rect(Screen.width - 500, 90, 100, 20), "[HOME/END] Max Hover Height: ", green);
            GUI.Label(new Rect(Screen.width - 310, 91, 100, 20), $"{_DebugVehicle.GetMaxHoverHeight().ToString("0.0")}", yellow);

            GUI.Label(new Rect(Screen.width - 500, 110, 100, 20), "[F11] Reset Settings", green);

            GUI.Label(new Rect(Screen.width - 500, 130, 100, 20), "[F12] Restart", green);

            if (_DebugCameraEnable)
            {
                GUI.Label(new Rect(Screen.width - 70, 31, 100, 20), "ON", green);

                GUI.Label(new Rect(Screen.width - 500, 150, 100, 20), "[W,A,S,D] Move Camera", green);

                GUI.Label(new Rect(Screen.width - 500, 170, 100, 20), "[LSHIFT] Boost Camera Speed", green);

                GUI.Label(new Rect(Screen.width - 500, 190, 100, 20), "[ALPHA1] Respawn at Camera", green);
            }
            else
            {
                GUI.Label(new Rect(Screen.width - 70, 31, 100, 20), "OFF", red);
            }

            if (_DebugPathEnable)
            {
                GUI.Label(new Rect(Screen.width - 70, 51, 100, 20), "ON", green);
            }
            else
            {
                GUI.Label(new Rect(Screen.width - 70, 51, 100, 20), "OFF", red);
            }
        }
        else
        {
            GUI.Label(new Rect(Screen.width - 70, 11, 100, 20), "OFF", red);
        }
    }
}
