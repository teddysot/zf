﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerNitrousBar : MonoBehaviour
{
    [SerializeField] private Slider _Slider;
    [SerializeField] private GameObject _FillTank;

    public void UpdateBar(float value)
    {

            if (value <= 0.1f)
            {
                _FillTank.SetActive(false);
            }
            else
            {
                _FillTank.SetActive(true);
            }

            _Slider.DOValue(value, 1.0f);
        }
}
