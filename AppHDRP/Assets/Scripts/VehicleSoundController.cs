﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSoundController : MonoBehaviour
{
    private VehicleMovement _VehicleMovement;
    private EnginePlayer _EnginePlayer;

    // Start is called before the first frame update
    void Start()
    {
        _VehicleMovement = GetComponent<VehicleMovement>();
        _EnginePlayer = GetComponent<EnginePlayer>();
        _EnginePlayer.PlayEngineSound();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _EnginePlayer.SetEngineSpeed(_VehicleMovement.GetSpeedPercentage());
        _EnginePlayer.SetEngineRoll(_VehicleMovement.GetAnglePercentage());
    }
}
