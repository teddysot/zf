﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerLapUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _Text;

    public void UpdateLap(int lap)
    {
        _Text.text = $"Lap {lap} / 3";
    }
}
