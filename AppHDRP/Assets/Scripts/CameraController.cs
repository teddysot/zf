using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _VirtualCamera;
    public CinemachineVirtualCamera VirtualCamera => _VirtualCamera;

    private void Start()
    {
        _VirtualCamera = GetComponentInChildren<CinemachineVirtualCamera>();

        _VirtualCamera.Follow = this.transform;
        _VirtualCamera.LookAt = this.transform;
    }
}