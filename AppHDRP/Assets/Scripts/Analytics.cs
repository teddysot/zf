﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Analytics : MonoBehaviourPunCallbacks
{
    private void Start() 
    {
        photonView.Owner.CustomProperties[CustomProperties.StatRing] = 0;
        photonView.Owner.CustomProperties[CustomProperties.StatNitrous] = 0;
        photonView.Owner.CustomProperties[CustomProperties.StatHit] = 0;
    }

    public void RingCollect()
    {
        photonView.Owner.CustomProperties[CustomProperties.StatRing] = (int)photonView.Owner.CustomProperties[CustomProperties.StatRing] + 1;
    }

    public void NitrousCollect()
    {
        photonView.Owner.CustomProperties[CustomProperties.StatNitrous] = (int)photonView.Owner.CustomProperties[CustomProperties.StatNitrous] + 1;
    }

    public void HitCollect()
    {
        photonView.Owner.CustomProperties[CustomProperties.StatHit] = (int)photonView.Owner.CustomProperties[CustomProperties.StatHit] + 1;
    }
}
