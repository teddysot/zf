# VFS Apps & Demos by Class #


## Each course/class is its own sub-module to keep them smaller and cleaner ##

* Not a single app, but a collection of apps in progress used as the basis for course work at VFS
** Social Integration - Wordpress, Facebook, Oauth
** Summer Intensive - Flappy Bird HTML5
** Web Apps 1 - HTML5 Client HTML5/CSS3/JS ES5
** Web Apps 2 - AJAX Server PHP and ajax
** Web Apps 3 - MVC Frameworks with AngularJS
** Web Apps 4 - Cloud Computing in Google App Engine
** Web Apps 5 - Performance
** Web Apps 6 - Hybrid Mobile/HTML5 Apps
